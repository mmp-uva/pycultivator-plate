# coding=utf-8
"""A module with all the classes for the 24-plate incubator data model"""


from pycultivator.data import DataModel

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PlateODRecord(DataModel.ODRecord):
    """A recording of the Optical Density in a well"""

    _namespace = "plate.data.record.od"
    _name = "PlateOdRecord"

    _schema = DataModel.ODRecord.mergeSchema(DataModel.Schema([
        DataModel.Variable("well", None, int, True),
    ]))

    def __init__(self, well, od_raw, wavelength, settings=None, properties=None, **kwargs):
        super(PlateODRecord, self).__init__(
            od_raw=od_raw, wavelength=wavelength, settings=settings, properties=properties, **kwargs
        )
        self["well"] = well


class PlateLightRecord(DataModel.LightRecord):
    """A recording of the light output in a well"""

    _namespace = "plate.data.record.light"
    _name = "LightRecord"

    _schema = DataModel.LightRecord.mergeSchema(DataModel.Schema([
        DataModel.Variable("well", None, int, True),
        DataModel.Variable("color", None, float, True),
        DataModel.Variable("light.dac", None, float),
    ]))

    def __init__(self, well, color, dac, intensity=None, settings=None, properties=None, **kwargs):
        super(PlateLightRecord, self).__init__(intensity=intensity, settings=settings, properties=properties, **kwargs)
        self["well"] = well
        self["color"] = color
        self["light.dac"] = dac

    def getWellNumber(self):
        return self["well"]

    def getDACValue(self):
        return self["light.dac"]

    def setIntensity(self, intensity):
        self["light.intensity"] = intensity
        return self.getIntensity()


class ODMeasurement(PlateODRecord, DataModel.TimeRecord):

    _namespace = "plate.data.measurement.od"
    _name = "OdMeasurement"

    _schema = DataModel.TimeRecord.mergeSchema(
        PlateODRecord.mergeSchema(
            {
                # add extra variables
            }
        )
    )

    def __init__(self, well, color, dac, t=None, t_zero=None, settings=None, properties=None, **kwargs):
        # add to kwargs so TimeRecord can pick it up
        kwargs["t"] = t
        kwargs["t_zero"] = t_zero
        super(ODMeasurement, self).__init__(
            well=well, color=color, dac=dac, settings=settings, properties=properties, **kwargs
        )
