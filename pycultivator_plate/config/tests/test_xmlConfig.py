"""
Module for testing the pyCultivator XML Configuration Objects
"""

from pycultivator.config.tests import test_xmlConfig
from pycultivator_plate.config import xmlConfig
from pycultivator.core import pcXML
from nose import SkipTest
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

SCHEMA_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "schema")
CONFIG_PATH = os.path.join(SCHEMA_DIR, "configuration.xml")
SCHEMA_PATH = os.path.join(SCHEMA_DIR, "pycultivator_plate.xsd")


class TestXMLPlateConnectionConfig(test_xmlConfig.TestXMLConnectionConfig):

    _abstract = False
    _config_cls = xmlConfig.XMLConfig
    _config_source = CONFIG_PATH
    _subject_cls = xmlConfig.XMLConnectionConfig

    @classmethod
    def getConfguration(cls):
        result = xmlConfig.XMLConfig.load(CONFIG_PATH)
        if result is None:
            raise ValueError("Unable to load configuration")
        return result

    def test_write(self):
        connection = self.getDefaultObject()
        ports = ["/dev/a", "/dev/b", "/dev/c"]
        connection._setPort(ports)
        # test write
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <connection></connection></a>""")
        elements = self.getSubject().getXML().getElementsByXPath("//pc:connection", root=xml, prefix="pc")
        self.assertGreaterEqual(len(elements), 1)
        element = elements[0]
        self.getSubject().write(connection, element)
        # now check if we can find the polynomial
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:board", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 3)
        for idx, element in enumerate(elements):
            self.assertEqual(ports[idx], element.get("port"))
        # test update
        ports = ["/dev/x", "/dev/y", "/dev/z"]
        connection._setPort(ports)
        elements = self.getSubject().getXML().getElementsByXPath("//pc:connection", root=xml, prefix="pc")
        self.assertGreaterEqual(len(elements), 1)
        element = elements[0]
        self.getSubject().write(connection, element)
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:board", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 3)
        for idx, element in enumerate(elements):
            self.assertEqual(ports[idx], element.get("port"))
