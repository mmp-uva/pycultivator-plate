"""This module provides an API for serializing the configuration setting of the 24-well plate incubator"""

from pycultivator.config import xmlConfig as baseXMLConfig
import baseConfig
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class XMLPlateConfig(baseXMLConfig.XMLComplexDeviceConfig, baseConfig.PlateConfig):
    """Config class to create/save/update the device objects from/to the configuration source"""

    XML_ELEMENT_TAG = "plate"
    required_source = "pycultivator_plate.config.xmlConfig.XMLConfig"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLPlateConfig, cls).assert_source(source, expected)


class XMLConnectionConfig(baseXMLConfig.XMLConnectionConfig, baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a LabJack connection object from a configuration source"""

    required_source = "pycultivator_plate.config.xmlConfig.XMLConfig"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLConnectionConfig, cls).assert_source(source, expected)

    def read_boards(self, definition, obj):
        # load board elements
        elements = self.find_elements("board", root=definition)
        for element in elements:
            index, port = self.read_board(element, obj, None)
            if None not in [index, port]:
                obj._setBoardPort(index, port)
        return obj

    def read_board(self, definition, parent=None, template=None):
        index = self.getXML().getElementId(definition, _type=int, default=0)
        port = self.getXML().getElementAttribute(definition, "port", _type=str)
        return index, port

    def write_board(self, index, port, definition):
        """Write board information to the configuration source

        :param index: The index of the board
        :param port: The port at which the board is connected
        :param definition: The definition to add the board information to
        :type definition: lxml.etree._Element._Element
        :return:
        """
        element = self.find_element("board", identifier=str(index))
        if element is not None:
            definition.remove(element)
        # create new element
        element = self.xml.addElement(definition, "board")
        self.xml.setElementId(element, index)
        self.xml.setElementAttribute(element, "port", port)
        return True


class XMLWellConfig(baseXMLConfig.XMLChannelConfig, baseConfig.WellConfig):
    """Config class to handle the channel configuration from the configuration source"""

    XML_ELEMENT_TAG = "well"
    required_source = "pycultivator_plate.config.xmlConfig.XMLConfig"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLWellConfig, cls).assert_source(source, expected)


class XMLInstrumentConfig(baseXMLConfig.XMLInstrumentConfig, baseConfig.baseConfig.InstrumentConfig):

    required_source = "pycultivator_plate.config.xmlConfig.XMLConfig"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLInstrumentConfig, cls).assert_source(source, expected)


class XMLSimpleInstrumentConfig(XMLInstrumentConfig):

    def read_properties(self, definition, obj):
        obj = super(XMLInstrumentConfig, self).read_properties(definition, obj)
        """:type: pycultivator_plate.instrument.PlateInstrument.PlateInstrument"""
        # read address
        address = self.getXML().getElementAttribute(definition, "address", _type=int)
        if address is not None:
            obj.setAddress(address)
        return obj

    def write_properties(self, obj, definition):
        result = super(XMLInstrumentConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementAttribute(definition, "address", obj.address)
        return result


class XMLLEDConfig(XMLSimpleInstrumentConfig, baseConfig.LEDConfig):
    """Config class to handle the LED configuration from the configuration source"""

    XML_ELEMENT_TAG = "led"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLLEDConfig, cls).assert_source(source, expected)

    def read_properties(self, definition, obj):
        obj = super(XMLLEDConfig, self).read_properties(definition, obj)
        """:type: pycultivator_plate.instrument.PlateLED.PlateLED"""
        color = self.xml.getElementAttribute(definition, "color", _type=str)
        if color is not None:
            obj.setColor(color)
        return obj

    def write_properties(self, obj, definition):
        """Writes LED properties to the configuration source

        :type obj: pycultivator_plate.instrument.plateLED.PlateLED
        :type definition: lxml.etree._Element._Element
        :return: bool
        """
        result = super(XMLLEDConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementAttribute(definition, "color", obj.color)
        return result


class XMLODLEDConfig(XMLSimpleInstrumentConfig, baseConfig.ODLEDConfig):
    """Config class to handle configuration of OD LEDs from an XML configuration source"""

    XML_ELEMENT_TAG = "od_led"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLODLEDConfig, cls).assert_source(source, expected)


class XMLDiodeConfig(XMLSimpleInstrumentConfig, baseConfig.DiodeConfig):
    """Config class to handle the OD Sensor configuration from the configuration source"""

    XML_ELEMENT_TAG = "diode"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLDiodeConfig, cls).assert_source(source, expected)


class XMLODConfig(XMLSimpleInstrumentConfig, baseConfig.ODConfig):

    XML_ELEMENT_TAG = "od"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLODConfig, cls).assert_source(source, expected)


class XMLFluorescenceConfig(XMLSimpleInstrumentConfig, baseConfig.FluorescenceConfig):
    """Config class to handle the LED configuration from the configuration source"""

    XML_ELEMENT_TAG = "fluorescence"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLFluorescenceConfig, cls).assert_source(source, expected)

    # Behaviour methods

    def read_properties(self, definition, obj):
        obj = super(XMLFluorescenceConfig, self).read_properties(definition, obj)
        """:type: pycultivator_plate.instrument.PlateFluorescence.PlateFluorescence"""
        # sensor = self.getXML().getAttribute(element, "sensor-address", vtype=int, default=None)
        # led = self.getXML().getAttribute(element, "led-address", vtype=int, default=None)
        # if sensor is not None:
        #     f.setEmissionSensor(sensor)
        # if led is not None:
        #     f.setExcitationLED(led)
        return obj

    def write_properties(self, obj, definition):
        result = super(XMLFluorescenceConfig, self).write_properties(obj, definition)
        if result:
            pass
            # self.xml.setElementAttribute(definition, "sensor-address", obj.getEmissionSensor())
            # self.xml.setElementAttribute(definition, "led-address", obj.getExcitationLED())
        return result


class XMLConfig(baseXMLConfig.XMLConfig, baseConfig.Config):
    """Config class for handling the Light Calibration Plate XML File"""

    # set available helpers
    _known_helpers = [
        # baseXMLConfig.XMLObjectConfig,
        # baseXMLConfig.XMLPartConfig,
        XMLPlateConfig,
        XMLConnectionConfig,
        XMLWellConfig,
        XMLLEDConfig,
        XMLDiodeConfig,
        XMLODLEDConfig,
        XMLODConfig,
        XMLFluorescenceConfig,
        baseXMLConfig.XMLCalibrationConfig,
        baseXMLConfig.XMLPolynomialConfig,
        baseXMLConfig.XMLSettingsConfig,
    ]

    XML_SCHEMA_PATH = os.path.join("schema", "pycultivator_plate.xsd")
    XML_SCHEMA_COMPATIBILITY = {
        "1.1": 2,
        "1.0": 0,
        # version: level
        # level 2 = full support
        # level 1 = deprecated
        # level 0 = not supported
    }

    @classmethod
    def load(cls, path, settings=None, **kwargs):
        """Loads the source into memory and creates the config object

        :param path: Location from which the configuration will be loaded
        :type path: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_plate.config.xmlConfig.Config
        """
        return super(XMLConfig, cls).load(path, settings, **kwargs)


class XMLConfigException(baseConfig.ConfigException):
    """An Exception raised by the XMlConfig classes"""

    def __init__(self, msg):
        super(XMLConfigException, self).__init__(msg)
