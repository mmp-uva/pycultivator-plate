"""This module provides an API for serializing the configuration setting of the 24-well plate incubator"""

from pycultivator.config import baseConfig
from pycultivator_plate import plate
from pycultivator_plate.connection import plateConnection
from pycultivator_plate.instrument import plateLED, plateODLED, plateDiode, plateFluorescence, plateOD

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PlateConfig(baseConfig.ComplexDeviceConfig):
    """Config class to create/save/update the device objects from/to the configuration source"""

    _configures = plate.Plate
    _configures_types = {"plate"}
    required_source = "pycultivator_plate.config.baseConfig.Config"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(PlateConfig, cls).assert_source(source, expected)


class ConnectionConfig(baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a connection object from a configuration source"""

    _configures = plateConnection.PlateConnection
    required_source = "pycultivator_plate.config.baseConfig.Config"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ConnectionConfig, cls).assert_source(source, expected)

    def read(self, definition, obj, **kwargs):
        obj = super(ConnectionConfig, self).read(definition, obj, **kwargs)
        # read boards
        obj = self.read_boards(definition, obj)
        return obj

    def read_boards(self, definition, obj):
        raise NotImplementedError

    def write(self, obj, definition):
        result = super(ConnectionConfig, self).write(obj, definition)
        if result:
            result = self.write_boards(obj, definition)
        return result

    def write_boards(self, obj, definition):
        result = True
        boards = obj.getBoardPorts()
        for index, port in enumerate(boards):
            result = self.write_board(index, port, definition) and result
        return result

    def write_board(self, index, port, definition):
        raise NotImplementedError


class WellConfig(baseConfig.ChannelConfig):
    """Config class to handle the channel configuration from the configuration source"""

    _configures = plate.PlateWell
    _configures_type = {"well"}
    required_source = "pycultivator_plate.config.baseConfig.Config"

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, Config):
            raise ConfigException("Expected a 24-well plate Configuration Source")
        super(WellConfig, self).__init__(source=source, settings=settings, **kwargs)

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(WellConfig, cls).assert_source(source, expected)

    # read methods

    def read_instruments(self, definition, obj, template=None):
        # load the led instruments
        ledHelper = self.getSource().getHelperFor("led")
        """:type: pycultivator_plate.config.XMLConfig.XMLLEDConfig"""
        odledHelper = self.getSource().getHelperFor("od_led")
        """:type: pycultivator_plate.config.XMLConfig.XMLODLEDConfig"""
        diodeHelper = self.getSource().getHelperFor("diode")
        """:type: pycultivator_plate.config.XMLConfig.XMLPhotoDiodeConfig"""
        odHelper = self.getSource().getHelperFor("od")
        """:type: pycultivator_plate.config.XMLConfig.XMLODConfig"""
        fluHelper = self.getSource().getHelperFor("fluorescence")
        """:type: pycultivator_plate.config.XMLConfig.XMLFluorescenceConfig"""
        instruments = []
        if ledHelper is not None:
            instruments.extend(ledHelper.load_all(root=definition, parent=obj, template=template))
        if odledHelper is not None:
            instruments.extend(odledHelper.load_all(root=definition, parent=obj, template=template))
        if diodeHelper is not None:
            instruments.extend(diodeHelper.load_all(root=definition, parent=obj, template=template))
        if odHelper is not None:
            instruments.extend(odHelper.load_all(root=definition, parent=obj, template=template))
        if fluHelper is not None:
            instruments.extend(fluHelper.load_all(root=definition, parent=obj, template=template))
        # add all found instruments
        for instrument in instruments:
            obj.setInstrument(instrument.getName(), instrument)
        return obj


class InstrumentConfig(baseConfig.InstrumentConfig):
    """Config class to handle the configuration of 24-plate instruments"""

    required_source = "pycultivator_plate.config.baseConfig.Config"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(InstrumentConfig, cls).assert_source(source, expected)

    def read(self, definition, obj, parent=None, **kwargs):
        obj = super(InstrumentConfig, self).read(definition, obj, parent=parent, **kwargs)

        return obj


class LEDConfig(baseConfig.InstrumentConfig):
    """Config class to handle the LED configuration from the configuration source"""

    _configures = plateLED.PlateLED
    _configures_types = {"led"}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(LEDConfig, cls).assert_source(source, expected)


class ODLEDConfig(baseConfig.InstrumentConfig):
    """Config class to handle the configuration of an OD LED"""

    _configures = plateODLED.PlateODLED
    _configures_types = {"od_led", "od-led", "odled"}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ODLEDConfig, cls).assert_source(source, expected)


class DiodeConfig(baseConfig.InstrumentConfig):
    """Config class to handle the PhotoDiode configuration from the configuration source"""

    _configures = plateDiode.PlateDiode
    _configures_types = {"diode"}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(DiodeConfig, cls).assert_source(source, expected)


class ODConfig(baseConfig.InstrumentConfig):
    """Config class to handle the OD sensor configuration from the configuration source"""

    _configures = plateOD.PlateOD
    _configures_types = {"od"}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ODConfig, cls).assert_source(source, expected)


class FluorescenceConfig(baseConfig.InstrumentConfig):
    """Config class to handle the Fluorescence sensor configuration from the configuration source"""

    _configures = plateFluorescence.PlateFluorescence
    _configures_types = {"fluorescence"}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(FluorescenceConfig, cls).assert_source(source, expected)


class Config(baseConfig.Config):
    """ The config class provides the API to configure a light calibration plate object."""

    namespace = "config"
    # set available helpers
    _known_helpers = [
        PlateConfig,
        ConnectionConfig,
        WellConfig,
        LEDConfig,
        ODLEDConfig,
        DiodeConfig,
        ODConfig,
        FluorescenceConfig,
        baseConfig.CalibrationConfig,
        baseConfig.PolynomialConfig,
        baseConfig.SettingsConfig
    ]

    default_settings = {
        # default settings for this class
    }

    def __init__(self, settings=None, **kwargs):
        super(Config, self).__init__(settings, **kwargs)

    @classmethod
    def load(cls, location, settings=None, **kwargs):
        """ABSTRACT Loads the source into memory and creates the config object

        :param location: Location from which the configuration will be loaded
        :type location: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_plate.config.baseConfig.Config
        """
        return super(Config, cls).load(location, settings=settings, **kwargs)


class ConfigException(baseConfig.ConfigException):
    """Exception raised in by pycultivator_plate.config.baseConfig classes"""

    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)
