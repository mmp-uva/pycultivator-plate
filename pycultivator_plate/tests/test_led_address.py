"""Test class for testing OD Address configuration"""

import os
from time import sleep

from pycultivator_plate.tests import LivePlateTestCase

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class LEDAddressTestCase(LivePlateTestCase):
    """Class for testing"""

    TEST_COLOR = "all"
    TEST_INTENSITY = 2000
    TEST_THRESHOLD = 1
    TEST_PAUSE = 0.1
    TEST_RANGE = range(0, 24)   # first plate

    LCP_CONFIG_PATH = os.path.join(
        os.path.dirname(__file__), "..", "..", "..", "pycultivator-plate-lcp", "examples", "configuration.xml"
    )

    _lc_plate = None

    @classmethod
    def getLCPlate(cls):
        """Returns the Light Calibration Plate

        :return: Light Calibration Plate instance
        :rtype: None or pycultivator_plate_lcp.device.PlateLCP.PlateLCP
        """
        return cls._lc_plate

    @classmethod
    def setLCPlate(cls, plate):
        cls._lc_plate = plate
        return cls.getLCPlate()

    @classmethod
    def hasLCPlate(cls):
        return cls.getLCPlate() is not None

    @classmethod
    def loadLCPlate(cls, path=None, settings=None, **kwargs):
        result = False
        if path is None:
            path = cls.LCP_CONFIG_PATH
        try:
            from pycultivator_plate_lcp.config import xmlConfig
        except ImportError:
            from unittest import SkipTest
            raise SkipTest("Light Calibration plate is not installed so skipping this test")
        c = xmlConfig.XMLConfig.load(path, settings=settings, **kwargs)
        if c is not None:
            # create device configuration object
            xdc = c.getHelperFor("device")
            # create device object
            d = xdc.load()
            """:type: pycultivator_plate_lcp.device.LCPlate.LCPlate"""
            if d is not None:
                cls.setLCPlate(d)
                result = cls.hasLCPlate()
        return result

    @classmethod
    def setUpClass(cls):
        """Prepares the class for running"""
        super(LEDAddressTestCase, cls).setUpClass()
        # determine which plate to test
        if os.getenv("USE_PLATE", "0") == "1":
            # use wells 24-48
            cls.TEST_RANGE = range(24, 48)
        else:
            cls.TEST_RANGE = range(24)
        cls.report("###################################")
        cls.report("This test uses the Light Calibration Plate")
        cls.report("Make sure it positioned correctly")
        cls.report("Will measure WELLS {} TO {}".format(min(cls.TEST_RANGE), max(cls.TEST_RANGE)))
        cls.report("Connection will {} simulated".format("BE" if cls.usesFake() else "NOT BE"))
        cls.report("###################################")
        # see if a fake connection should be made
        # load configuration classes
        cls.loadLCPlate(settings={"fake.connection": cls.usesFake()})
        if not cls.hasLCPlate():
            raise AssertionError("Unable to load LC Plate")
        if not cls.getLCPlate().connect():
            raise AssertionError("Unable to connect to plate")

    @classmethod
    def tearDownClass(cls):
        if cls.getLCPlate().hasConnection():
            result = cls.getLCPlate().disconnect()
            if not result:
                raise AssertionError("Unable to disconnect plate")
        super(LEDAddressTestCase, cls).tearDownClass()

    def test_LEDAddresses(self):
        """Method that tests each well"""
        for well in self.TEST_RANGE:
            for color in self.getTestColors():
                with self.subTest("WELL {} | COLOR {}".format(well, color)):
                    self.check_well(well, color)

    def check_well(self, well_idx, color):
        """"""
        self.getLog().info("Testing well {}".format(well_idx))
        diode = self.getLCPDiode(well_idx)
        self.assertIsNotNone(diode, msg="Unable to obtain PhotoDiode object")
        # get blank
        blank = diode.measure().getVoltage()
        # turn light on
        self.getPlate().getChannel(well_idx).setLightIntensity(color, self.TEST_INTENSITY)
        # pause
        sleep(self.TEST_PAUSE)
        # measure signal
        signal = diode.measure().getVoltage()
        # turn light off
        self.getPlate().getChannel(well_idx).setLightIntensity(color, 0)
        # report
        self.report(
            "WELL={} | COLOR={} | Blank={:.6f} | Signal={:.6f} | Norm={:.6f} | Ratio={:.6f} | THRESHOLD={:.2f}".format(
                well_idx, color, blank, signal, signal - blank, signal / blank, self.TEST_THRESHOLD
            )
        )
        # determine success
        self.assertTrue(
            signal / blank > self.TEST_THRESHOLD,
            msg="Signal Ratio ({:.6f}) in Well {} not above threshold ({:.2f})".format(
                signal / blank, well_idx, self.TEST_THRESHOLD
            )
        )
        # finished

    def getLCPDiode(self, well_idx):
        """

        :param well_idx:
        :type well_idx:
        :return:
        :rtype: pycultivator_plate_lcp.instrument.Diode.Diode
        """
        result = None
        well = self.getLCPlate().getChannel(well_idx)
        sensors = well.getDiodes().values()
        if len(sensors) > 0:
            result = sensors[0]
        return result

    @classmethod
    def getTestColors(cls):
        colors = cls.TEST_COLOR
        if not isinstance(colors, (tuple ,list)):
            colors = [colors]
        if "all" in colors:
            colors = ["red", "green", "blue", "white"]
        return colors
