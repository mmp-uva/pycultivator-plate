"""A global test script to test the functioning of the whole package"""

from pycultivator_plate.tests import LivePlateTestCase

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class PackageTestCase(LivePlateTestCase):
    """Tests the package"""

    ALL_WELLS = range(48)
    ALL_LEDS = range(191) # 191
    TEST_LED_INTENSITY = 1000
    ALL_OD_LEDS = range(1) # 48
    TEST_OD_LED_INTENSITY = 1000
    ALL_PHOTO_DIODES = range(1) # 48

    def test_wells(self):
        """Tests is we can use each well"""
        plate = self.getPlate()
        for well_idx in self.ALL_WELLS:
            with self.subTest(well=well_idx):
                well = plate.getChannel(well_idx)
                self._test_well(well)

    def _test_well(self, well):
        self.assertIsNotNone(well)
        # should have 4 leds
        self.assertEqual(well.countInstrumentsOfType("led"), 4)
        self.assertEqual(well.countInstrumentsOfType("od-led"), 1)
        self.assertEqual(well.countInstrumentsOfType("photodiode"), 1)
        # self.assertEqual(well.countInstrumentsOfType("od"), 1)
        # self.assertEqual(well.countInstrumentsOfType("fluorescence"), 1)

    def test_leds(self):
        """Test if we can set every LED"""
        # make rainbow
        for led_idx in self.ALL_LEDS:
            with self.subTest(msg="Turn on", led=led_idx):
                self.assertTrue(self.apply_led(led_idx, self.TEST_LED_INTENSITY))
        # turn off
        for led_idx in self.ALL_LEDS:
            with self.subTest(msg="Turn off", led=led_idx):
                self.assertTrue(self.apply_led(led_idx, 0))

    def test_od_leds(self):
        # turn on
        for od_led_idx in self.ALL_OD_LEDS:
            with self.subTest(msg="Turn on", od_led=od_led_idx):
                self.assertTrue(self.apply_od_led(od_led_idx, self.TEST_OD_LED_INTENSITY))
        # turn off
        for od_led_idx in self.ALL_OD_LEDS:
            with self.subTest(msg="Turn off", od_led=od_led_idx):
                self.assertTrue(self.apply_od_led(od_led_idx, 0))

    def test_photo_diode(self):
        # read
        for diode in self.ALL_PHOTO_DIODES:
            with self.subTest(diode=diode):
                self.read_photo_diode(diode)

    def apply_well_led(self, well_idx, color, intensity):
        well = self.getPlate().getChannel(well_idx)
        self.assertIsNotNone(well)
        leds = well.getLEDsOfColor(color)
        self.assertTrue(len(leds) > 0)
        leds[0].setDACIntensity(intensity)

    def apply_led(self, led_idx, intensity):
        return self.getPlate().getConnection().setLEDIntensity(
            led_idx, intensity
        )

    def apply_od_led(self, od_led_idx, intensity):
        return self.getPlate().getConnection().setODIntensity(od_led_idx, intensity) and \
            self.getPlate().getConnection().setODState(od_led_idx, intensity > 0)

    def read_photo_diode(self, diode_idx):
        return self.getPlate().getConnection().readPhotoDiode(diode_idx)

    def test_instruments(self):
        i_types = self.getPlate().getInstrumentTypes()
        self.assertEqual(
            i_types,
            {"led", "light", "diode", "photodiode", "od", "odled", "od_led", "od-led", "fluorescence"}
        )

    def test_measures(self):
        variables = self.getPlate().measures()
        self.assertEqual(
            variables, {
                "state", "intensity", "raw_intensity", "dac_intensity", "voltage_intensity",
                "od", "od_raw", "od_signal", "od_flash", "od_background",
                "fluorescence",
                "excitation_raw_intensity", "excitation_dac_intensity", "excitation_intensity",
                "excitation_time", "emission_time"
            }
        )

    def test_measure(self):
        measurements = self.getPlate().measure("photodiode", "voltage_intensity")
        self.assertTrue(len(measurements) > 0)

    def test_control(self):
        successes = self.getPlate().control("photodiode", "voltage_intensity", 100)
        for success in successes:
            self.assertFalse(success)

    def test_controls(self):
        variables = self.getPlate().controls()
        self.assertEqual(
            variables, {
                "state", "intensity", "raw_intensity", "dac_intensity",
                "excitation_raw_intensity", "excitation_dac_intensity", "excitation_intensity",
                "excitation_time"
            }
        )

    def test_getPhotoDiode(self):
        self.assertIsNotNone(
            self.getPlate().getChannel(0).getPhotoDiode()
        )

    def test_getLEDs(self):
        self.assertTrue(
            len(self.getPlate().getChannel(0).getLEDs()) > 0
        )

    def test_getODLED(self):
        self.assertIsNotNone(
            self.getPlate().getChannel(0).getODLED()
        )

    def test_getFluorescence(self):
        self.assertIsNotNone(
            self.getPlate().getChannel(0).getFluorescenceInstrument()
        )
