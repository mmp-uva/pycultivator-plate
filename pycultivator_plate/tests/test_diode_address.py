"""Test class for testing OD Address configuration"""

from time import sleep

from pycultivator_plate.tests import LivePlateTestCase
from pycultivator_plate.instrument import PlateDiode

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class DiodeAddressTestCase(LivePlateTestCase):
    """Class for testing"""

    TEST_COLOR = "white"
    TEST_INTENSITY = 1000
    TEST_THRESHOLD = 1.1
    TEST_PAUSE = 0.1

    # _log_level = _test._test.uvaLog.LEVELS["INFO"]

    def test_DiodeAddresses(self):
        """Method that tests each well"""
        wells = self.getPlate().getChannels().keys()
        for well in wells:
            with self.subTest(well):
                self.check_well(well)

    def check_well(self, well_idx):
        """"""
        self.getLog().info("Testing well {}".format(well_idx))
        sensor = self.getDiode(well_idx)
        self.assertIsInstance(sensor, PlateDiode, msg="Received invalid PhotoDiode object")
        # gather leds of desired color
        leds = self.getPlate().getChannel(well_idx).getLEDsOfColor(self.TEST_COLOR).values()
        self.assertGreater(len(leds), 0)
        # Make sure sensor can perform action
        self.assertTrue(sensor.canMeasure("intensity"))
        # light off
        self.getPlate().getChannel(well_idx).setLightIntensity(0, leds)
        sleep(self.TEST_PAUSE)
        # get blank
        blank = sensor.measure("intensity")
        # turn light on
        self.getPlate().getChannel(well_idx).setLightIntensity(self.TEST_INTENSITY, leds)
        # pause
        sleep(self.TEST_PAUSE)
        # measure signal
        signal = sensor.measure("intensity")
        # turn light off
        self.getPlate().getChannel(well_idx).setLightIntensity(0, leds)
        # report
        self.report(
            "WELL={:2d} | SENSOR={:2d} | Blank={:.6f} | Signal={:.6f} | Ratio={:.6f} | THRESHOLD={:.2f}".format(
                well_idx, sensor.getAddress(), signal, blank, signal / blank, self.TEST_THRESHOLD
            )
        )
        # determine success
        if not self.getPlate().getConnection().isFake():
            self.assertGreater(
                signal / blank, self.TEST_THRESHOLD,
                msg="Signal Ratio ({:.6f}) in Well {} not above threshold ({:.2f})".format(
                    signal / blank, well_idx, self.TEST_THRESHOLD
                )
            )
        # finished

    def getDiode(self, well_idx):
        result = None
        well = self.getPlate().getChannel(well_idx)
        """:type: pycultivator_plate.plate.PlateWell"""
        if well is not None:
            result = well.getPhotoDiode()
        return result
