"""Core test classes

Handles basic configuration
"""

from pycultivator.core.tests import UvaTestCase
from pycultivator_plate.config import xmlConfig
import os, sys, pkg_resources

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class LivePlateTestCase(UvaTestCase):
    """TestCase class for live functionality tests, these tests require a working plate"""

    COLORS = ["white", "green", "red", "blue", "all"]

    PLATE_CONFIG_PATH = pkg_resources.resource_filename(
        __name__, os.path.join("..", "config", "schema", "configuration.xml")
    )

    _use_fake = os.getenv("USE_FAKE", "1") == "1"
    _plate = None

    @staticmethod
    def normalize(signals, blanks):
        results = []
        if not isinstance(signals, (tuple ,list)):
            signals = [signals]
        if not isinstance(blanks, (tuple, list)):
            blanks = [blanks]
        for idx, signal in enumerate(signals):
            blank = 0.0 if idx > len(blanks) else blanks[idx]
            results.append(signal - blank)
        return results

    @classmethod
    def loadPlate(cls, path=None, settings=None, **kwargs):
        """ Loads the configuration object and device from the given XML Configuration file path into a new script

        :param path: Path to the XML Configuration file
        :type path: str
        :param settings: Settings to be passed to the configuration
        :type settings: None or dict
        :return:
        :rtype: pycultivator_plate.device.Plate.Plate
        """
        result = False
        if path is None:
            path = cls.PLATE_CONFIG_PATH
        c = xmlConfig.XMLConfig.load(path, settings=settings, **kwargs)
        if c is not None:
            # create device configuration object
            xdc = c.getHelperFor("device")
            # create device object
            d = xdc.load()
            """:type: pycultivator_plate.device.Plate.Plate"""
            if d is not None:
                cls.setPlate(d)
                result = cls.hasPlate()
        return result

    @classmethod
    def getPlate(cls):
        """Return the plate object or nothing if no plate is set

        :rtype: pycultivator_plate.plate.Plate or None
        """
        return cls._plate

    @classmethod
    def hasPlate(cls):
        """Returns whether a plate is set for this test

        :rtype: bool
        """
        return cls.getPlate() is not None

    @classmethod
    def setPlate(cls, plate):
        """Sets the plate object that is used in this test

        :type plate: pycultivator_plate.device.Plate.Plate
        """
        cls._plate = plate
        return cls.getPlate()

    @classmethod
    def usesFake(cls):
        """Whether this test uses a fake plate connection, thus simulating a plate"""
        return cls._use_fake

    @classmethod
    def getWells(cls):
        """Return a list of wells in the plate, if a plate is set

        :rtype: None or list[pycultivator_plate.device.Plate.PlateWell]
        """
        result = None
        if cls.hasPlate():
            result = cls.getPlate().getChannels().values()
        return result

    @classmethod
    def setUpClass(cls):
        """Prepares the class for running"""
        super(LivePlateTestCase, cls).setUpClass()
        # see if a fake connection should be made
        cls.getLog().info(
            "Will {} simulate the connection to the 24-Well Plate".format("NOT" if cls.usesFake() else "")
        )
        # load configuration classes
        cls.loadPlate(settings={"fake.connection": cls.usesFake()})
        if not cls.hasPlate():
            raise AssertionError("Unable to load configuration file")
        result = cls.getPlate().connect()
        if not result:
            raise AssertionError("Unable to connect to plate")

    @classmethod
    def tearDownClass(cls):
        if cls.getPlate().isConnected():
            # result = cls.blankPlate()
            # if not result:
            #     raise AssertionError("Unable to blank plate")
            result = cls.getPlate().disconnect()
            if not result:
                raise AssertionError("Unable to disconnect plate")

    @classmethod
    def blankPlate(cls):
        wells = cls.getPlate().getChannels().values()
        result = len(wells) > 0
        for well in wells:
            colors = well.getLEDColors()
            result = len(colors) > 0 and result
            for color in colors:
                result = well.setLightIntensity(color, 0) and result
        return result

    @classmethod
    def report(cls, msg, end="\n"):
        sys.stderr.write("{}{}".format(msg, end))
        sys.stderr.flush()
