
import os
import pkg_resources


CONFIG_PATH = pkg_resources.resource_filename(
    __name__, os.path.join("..", "config", "schema", "configuration.xml")
)
USE_FAKE = os.getenv("USE_FAKE", "1") == "1"


if __name__ == "__main__":
    location = CONFIG_PATH
    print("Import xmlConfig")
    from pycultivator.config import xmlConfig

    print("Load xmlConfig object")
    config = xmlConfig.XMLConfig.load(location)

    if config is None:
        print ("Unable to load configuration: {}".format(location))

    print("Load device config helper")
    config.setSetting("fake.connection", USE_FAKE, namespace="")
    xdc = config.getHelperFor("device", settings={"fake.connection": USE_FAKE})
    device = None

    print("Load device object")
    from pycultivator.core import pcException
    try:
        device = xdc.load()
    except pcException.PCException as pce:
        print("Unable to load Device: {}".format(pce))

    print ("Connect to plate")
    device.connect()
    try:
        print ("Demonstrate light intensity")
        for well_idx in device.getWells():
            well = device.getWell(well_idx)
            for led in well.getLEDs().values():
                led.setIntensity(200, True)

        print ("Sleep (60 seconds)")
        import time
        time.sleep(60)

        print ("Restore to darkness")
        for well_idx in device.getWells():
            well = device.getWell(well_idx)
            for led in well.getLEDs().values():
                led.setIntensity(0, True)
    except KeyboardInterrupt:
        print ("Exit!")
    finally:
        device.disconnect()

    print("Done")
