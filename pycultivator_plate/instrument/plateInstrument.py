"""
The PlateInstrument Module provides basic classes for working with Instruments in a 24 well plate
"""

from pycultivator.instrument import Instrument, InstrumentException

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PlateInstrument(Instrument):
    """The PlateInstrument models an Instrument in the 24 well plate incubator"""

    namespace = "plate.instrument"

    default_settings = {
        # default settings for this class
    }

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PlateInstrument, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)
        self._address = None

    # Getters / Setters

    def hasWell(self):
        return self.hasParent()

    def getWell(self):
        """Returns the well object to which this instrument belongs.

        :rtype: pycultivator_plate.plate.PlateWell
        """
        return self.getParent()

    @property
    def well(self):
        return self.getWell()

    def getWellIndex(self):
        """Returns the index of the well in the plate

        :rtype: int
        """
        if not self.hasParent():
            raise PlateInstrumentException("Instrument does not have a parent")
        return self.getParent().getIndex()

    @property
    def well_index(self):
        return self.getWellIndex()

    def hasPlate(self):
        return self.hasWell() and self.getWell().hasParent()

    def getPlate(self):
        """Returns the plate object to which this instrument belongs.

        :rtype: pycultivator_plate.plate.Plate
        """
        if not self.hasWell():
            raise PlateInstrumentException("Instrument does not have a parent")
        return self.getWell().getParent()



class PlateInstrumentException(InstrumentException):
    """Exception raised by a PlateDiode Instrument"""

    pass


class SimplePlateInstrument(PlateInstrument):
    """A Simple Plate Instrument: refers to a single sensor with an address"""

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(SimplePlateInstrument, self).__init__(identity, parent, settings, **kwargs)
        self._address = None

    def _getAddress(self):
        return self._address

    def hasAddress(self):
        """Returns whether this LED has a specifically set address.

        If false means; the result of getAddress is guessed using guessAddress

        :rtype: bool
        """
        return self._getAddress() is not None

    def getAddress(self):
        """Returns the address of this LED.

        If the address is not set in the settings, the address will be guessed using guessAddress

        :rtype: int
        """
        result = self._getAddress()
        if result is None:
            result = self.guessAddress()
        return result

    @property
    def address(self):
        return self.getAddress()

    def setAddress(self, address):
        """Sets a custom address for this LED (overrides guessing)

        :param address: The new address of the LED
        :type address: int
        """
        self._address = address

    def guessAddress(self):
        """Tries to guess the Sensor address from the index of the well where this instrument belongs to"""
        raise NotImplementedError

    def copy(self, memo=None):
        other = super(SimplePlateInstrument, self).copy(memo=memo)
        if isinstance(other, SimplePlateInstrument):
            other.setAddress(self._getAddress())
        return other
