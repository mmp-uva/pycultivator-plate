"""
The PlateODLED Module provides classes for working with the OD LED's in a 24 well plate
"""

from pycultivator.instrument.light import Light, LightException
from pycultivator.instrument import InstrumentVariable
import plateInstrument

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PlateODLED(plateInstrument.SimplePlateInstrument, Light):

    INSTRUMENT_TYPE_SYNONYMS = {
        "od_led", "odled", "od-led"
    }
    INSTRUMENT_TYPE_INHERITANCE = False

    namespace = "plate.instrument.od_led"
    default_settings = {
        # add default settings for this class
    }

    _dac_intensity = InstrumentVariable(
        None, private=True, can_measure=True, can_control=True, minimum=0
    )

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PlateODLED, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)
        self._raw_intensity.maximum = 4094
        self._dac_intensity.maximum = 4094
        self._color = "infrared"
        self._wavelength = 730

    #
    #    Properties
    #

    def getDACIntensity(self, update=False):
        """Returns the intensity in DAC values"""
        value = self.getRawIntensity(update=update)
        self._dac_intensity.set(value)
        return self._dac_intensity.get()

    @property
    def dac_intensity(self):
        return self.getDACIntensity(False)

    def setDACIntensity(self, value, update=False):
        """Sets the intensity of the LED in DAC values

        :param value: Intensity in DAC values
        :param update: Whether to update the internal intensity variable
        :rtype: bool
        """
        result = self.setRawIntensity(value, update=update)
        if result:
            self._dac_intensity.set(value)
            if update:
                self._dac_intensity.commit()
        return result

    @dac_intensity.setter
    def dac_intensity(self, v):
        self.setDACIntensity(v, update=False)

    #
    #   Communication hooks
    #

    def _measure(self, source, **kwargs):
        super(PlateODLED, self)._measure(source, **kwargs)
        if source is self._dac_intensity:
            self.getDACIntensity(True)

    def _control(self, source, value, **kwargs):
        super(PlateODLED, self)._control(source, value, **kwargs)
        if source is self._dac_intensity:
            self.setDACIntensity(value, True)

    def read_led_state(self):
        # return internal state as plate does not support reading
        return self.getState(False)

    def write_led_state(self, state):
        result = False
        if self.hasPlate() and self.getPlate().isConnected():
            connection = self.getPlate().getConnection()
            result = connection.setODState(self.getAddress(), state)
        return result

    def read_led_intensity(self):
        # return internal intensity as plate does not support reading
        return self.getRawIntensity(False)

    def write_led_intensity(self, value):
        result = False
        if self.hasPlate() and self.getPlate().isConnected():
            connection = self.getPlate().getConnection()
            result = connection.setODIntensity(self.getAddress(), value)
        return result

    #
    #   Behaviour
    #

    def guessAddress(self):
        """Guess the address of the OD LED based on other information known to this object

        NOT (YET) IMPLEMENTED: returns 0

        :rtype: int
        """
        return 0


class PlateODLEDException(plateInstrument.PlateInstrumentException, LightException):
    """Exception raised by a PlateODLED Instrument"""

    pass
