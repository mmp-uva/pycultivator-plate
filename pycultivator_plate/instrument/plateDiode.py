"""
The PlatePhotoDiode Module provides classes for working with the P in a 24 well plate
"""

from pycultivator.instrument.diode import Diode, InstrumentVariable
import plateInstrument

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PlateDiode(
    plateInstrument.SimplePlateInstrument, Diode
):
    """The PlateDiode class models a PhotoDiode in the 24-well plate"""

    INSTRUMENT_TYPE_SYNONYMS = {
        "photodiode"
    }

    namespace = "plate.instrument.diode"
    default_settings = {
        # default settings for this object
    }

    _voltage_intensity = InstrumentVariable(None, private=True)

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PlateDiode, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)
        # self._voltage_intensity.bindEvent("measure", lambda: self.getVoltageIntensity(True))

    # Getters / Setters

    def getVoltageIntensity(self, update=False):
        value = self.getRawIntensity(update=update)
        self._voltage_intensity.set(value)
        if update:
            self._voltage_intensity.commit()
        return value

    # Behaviour methods

    def guessAddress(self):
        """Tries to guess the address of the diode based on the well it is connect to etc.

        For photo diodes this is not possible, so it will return the currently known address.

        :return:
        :rtype: int
        """
        return 0

    # communication

    def _measure(self, source, **kwargs):
        super(PlateDiode, self)._measure(source, **kwargs)
        if source is self._voltage_intensity:
            self.getVoltageIntensity(True)

    def read_diode(self):
        if not self.hasPlate() or not self.getPlate().isConnected():
            raise PlateDiodeException("Unable to read PhotoDiode: no plate is not connected")
        return self.getPlate().getConnection().readPhotoDiode(self.getAddress())

    def read_diode_voltage(self, replicates=None):
        return self.read_diode_raw(replicates)


class PlateDiodeException(plateInstrument.PlateInstrumentException):
    """Exception raised by a PlateDiode Instrument"""

    pass
