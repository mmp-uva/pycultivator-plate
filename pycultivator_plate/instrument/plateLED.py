"""
The PlateLED Module provides classes for working with LED's in a 24 well plate
"""

from pycultivator.instrument.light import Light, LightException
from pycultivator.instrument import InstrumentVariable
import plateInstrument

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PlateLED(plateInstrument.SimplePlateInstrument, Light):
    """The PlateLED models a LED in the 24 well plate incubator"""

    INSTRUMENT_TYPE_SYNONYMS = {
        # add synonyms
        "led"
    }

    namespace = "plate.instrument.led"
    default_settings = {
        # add default settings for this class
    }

    LED_COLOR_WHITE = 0
    LED_COLOR_GREEN = 1
    LED_COLOR_RED = 2
    LED_COLOR_BLUE = 3

    LED_COLOR_NAME_MAP = {
        "blue": LED_COLOR_BLUE,
        "green": LED_COLOR_GREEN,
        "red": LED_COLOR_RED,
        "white": LED_COLOR_WHITE,
    }

    LED_COLOR_NAMES = LED_COLOR_NAME_MAP.keys()

    LED_COLOR_INDEX_MAP = {
        LED_COLOR_BLUE: "blue",
        LED_COLOR_GREEN: "green",
        LED_COLOR_RED: "red",
        LED_COLOR_WHITE: "white",
    }

    _dac_intensity = InstrumentVariable(
        None, private=True, can_measure=True, can_control=True, minimum=0
    )

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PlateLED, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)
        self._raw_intensity.maximum = 4094
        self._dac_intensity.maximum = 4094

    # Getters / Setters

    @classmethod
    def validateColorName(cls, name):
        """Returns whether the given name is a valid color name

        :type name: str
        :rtype: bool
        """
        return name.lower().strip() in cls.LED_COLOR_NAME_MAP.keys()

    @classmethod
    def validateColorIndex(cls, index):
        """Returns whether the given index is a valid color index

        :type index: int
        :rtype: bool
        """
        return index in cls.LED_COLOR_INDEX_MAP.keys()

    def getColor(self):
        """Returns the color of the LED as a string

        :rtype: str
        """
        return self.getColorName()

    def getColorIndex(self):
        """Returns the color of the LED as an integer (0, 1, 2 or 3)

        :rtype: int
        """
        return self._color

    @property
    def color_name(self):
        return self.getColorName()

    def getColorName(self):
        """Returns the color of the LED as a string (i.e. blue, green, red or white)"""
        index = self.getColorIndex()
        if not self.validateColorIndex(index):
            raise PlateLEDException("Invalid color index. {} - not known.".format(index))
        return self.LED_COLOR_INDEX_MAP[index]

    def setColor(self, value):
        if isinstance(value, int):
            self.setColorIndex(value)
        if isinstance(value, str):
            self.setColorName(value)

    def setColorName(self, name):
        """Sets the color of the LED by using a string

        :param name: The name of the new color of this LED
        :type name: str
        :return: The new color index
        :rtype: int
        """
        if not self.validateColorName(name):
            raise PlateLEDException("Invalid color name. {} - not known.".format(name))
        index = self.LED_COLOR_NAME_MAP[name]
        self.setColorIndex(index)

    def setColorIndex(self, index):
        """Sets the color of the LED by using an integer

        :param index: The index of the new color of this LED
        :type index: int
        :return: The new color index
        :rtype: int
        """
        if not self.validateColorIndex(index):
            raise PlateLEDException("Invalid color index. {} - not known.".format(index))
        self._color = index

    def emitsColor(self, color):
        """Returns whether this LED emits the given color name or index

        :type color: str | int
        :rtype: bool
        """
        result = False
        if isinstance(color, int):
            result = self.emitsColorIndex(color)
        if isinstance(color, str):
            result = self.emitsColorName(color)
        return result

    def emitsColorName(self, name):
        """Returns whether this LED emits the given color name

        :type name: str
        :rtype: bool
        """
        return self.getColorName() == name

    def emitsColorIndex(self, index):
        """Returns whether this LED emits the given color index

        :type index: int
        :rtype: bool
        """
        return self.getColorIndex() == index

    def getDACIntensity(self, update=False):
        """Returns the intensity in DAC values"""
        return self.getRawIntensity(update=update)

    @property
    def dac_intensity(self):
        value = self.getDACIntensity(False)
        self._dac_intensity.set(value)
        return self._dac_intensity.get()

    def setDACIntensity(self, value, update=False):
        """Sets the intensity of the LED in DAC values

        :param value: Intensity in DAC values
        :param update: Whether to update the internal intensity variable
        :rtype: bool
        """
        result = self.setRawIntensity(value, update=update)
        if result:
            self._dac_intensity.set(value)
            if update:
                self._dac_intensity.commit()
        return result

    @dac_intensity.setter
    def dac_intensity(self, v):
        self.setDACIntensity(v, update=False)

    #
    #   Communication hooks
    #

    def _measure(self, source, **kwargs):
        super(PlateLED, self)._measure(source, **kwargs)
        if source is self._dac_intensity:
            self.getDACIntensity(True)

    def _control(self, source, value, **kwargs):
        super(PlateLED, self)._control(source, value, **kwargs)
        if source is self._dac_intensity:
            self.setDACIntensity(value, True)

    def read_led_state(self):
        # return internal state as plate does not support reading
        return self.getRawIntensity(False) > 0

    def write_led_state(self, state):
        # result = False
        value = self.getRawIntensity(False) if state else 0
        return self.write_led_intensity(value)

    def read_led_intensity(self):
        # return internal intensity as plate does not support reading
        return self.getRawIntensity(False)

    def write_led_intensity(self, value):
        result = False
        if self.hasPlate() and self.getPlate().isConnected():
            connection = self.getPlate().getConnection()
            result = connection.setLEDIntensity(self.getAddress(), value)
        return result

    # Behaviour methods

    def guessAddress(self):
        """Learns the Sensor address from the index of the well where this instrument belongs to"""
        address = 0
        color = self.getColorIndex()
        well = self.getWellIndex()
        # well = 10, color = green = 1: 2 + (1/2)*4 + 1 % 2 + (10 % 2)*-2 + (10/2)*8 = 43
        # well = 11, color = green = 1: 2 + (1/2)*4 + 1 % 2 + (11 % 2)*-2 + (11/2)*8 = 41
        if None not in (color, well):
            address = 2 + (color/2)*4 + color % 2 + (well % 2)*-2 + (well/2)*8
        return address


class PlateLEDException(plateInstrument.PlateInstrumentException, LightException):
    """Exception raised by a PlateLED Instrument"""

    pass
