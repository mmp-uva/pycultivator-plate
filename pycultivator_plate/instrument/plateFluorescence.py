"""
The PlateFluorescence Module provides classes for working with the Fluorescence Sensor in a 24 well plate
"""

import plateInstrument, time
from pycultivator.instrument import InstrumentVariable

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PlateFluorescence(plateInstrument.PlateInstrument):
    """The PlateDiode models a ODSensor in the 24 well plate incubator"""

    INSTRUMENT_TYPE_SYNONYMS = {
        plateInstrument.PlateInstrument.INSTRUMENT_TYPE_FLUORESCENCE
    }

    namespace = "plate.instrument.fluorescence"
    default_settings = {
        "emission.address": None,
        "excitation.address": None,
        "excitation.time": 1,   # excitation time in seconds
        "excitation.intensity": 50,  # intensity of excitation signal
    }

    _fluorescence = InstrumentVariable(None, private=True)
    _emission_time = InstrumentVariable(None, private=True)
    _excitation_time = InstrumentVariable(0.1, private=True, can_control=True)
    _excitation_raw_intensity = InstrumentVariable(0, private=True, can_control=True)
    _excitation_dac_intensity = InstrumentVariable(0, private=True, can_control=True)
    _excitation_intensity = InstrumentVariable(0, private=True, can_control=True)

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PlateFluorescence, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)
        # TODO: Implement event bindings

    # Getters / Setters

    def getEmissionSensor(self):
        """Returns the address of the PhotoDiode used to measure the emitted fluorescence

        :rtype: int
        """
        return self.getSetting("emission.address")

    def setEmissionSensor(self, address):
        """Sets the address of the PhotoDiode used to measure the emitted fluorescence"""
        # TODO: Test boundaries on address
        self.setSetting("emission.address", address)
        return self.getEmissionSensor()

    def getExcitationLED(self):
        """Returns the address of the LED used to excite the well

        :rtype: None | int
        """
        return self.getSetting("excitation.address")

    def setExcitationLED(self, address):
        # TODO: Test boundaries on address
        self.setSetting("excitation.address", address)
        return self.getExcitationLED()

    def getExcitationTime(self):
        """Returns the amount of time to excite the well (i.e. how is the LED on?)"""
        return self.getSetting("excitation.time")

    def setExcitationTime(self, t):
        """Sets the amount of time to excite the well (i.e. how long is the LED on?)"""
        self.setSetting("excitation.time", t)
        return self.getExcitationTime()

    def getExcitationIntensity(self):
        """Sets the excitation intensity in DAC values"""
        return self.getSetting("excitation.intensity")

    def setExcitationIntensity(self, i):
        self.setSetting("excitation.intensity", i)
        return self.getExcitationIntensity()

    # Behaviour methods

    def guessExcitationLED(self):
        """Guesses the address of the LED used to excite the well"""
        result = None
        results = self.getWell().getLEDsOfColor("blue").values()
        if len(results) > 0:
            result = results[0].getAddress()
        return result

    def findExcitationLED(self):
        """Tries to find the proper LED first by looking at what is defined

        :rtype: None or pycultivator_plate.instrument.PlateLED.PlateLED
        """
        result = None
        if self.getExcitationLED() is not None:
            results = self.getWell().getLEDsAtAddress(self.getExcitationLED())
            if len(results.keys()) > 0:
                result = results[results.keys()[0]]
        elif self.guessExcitationLED() is not None:
            results = self.getWell().getLEDsAtAddress(self.guessExcitationLED())
            if len(results.keys()) > 0:
                result = results[results.keys()[0]]
        return result

    def guessEmissionSensor(self):
        """Guesses the address of the PhotoDiode used to measure the emitted fluorescence"""
        return None

    def guessAddress(self):
        """Learns the Sensor address from the index of the well where this instrument belongs to"""
        return self.guessExcitationLED(), self.guessEmissionSensor()

    # Behaviour

    def _measure(self, source, **kwargs):
        super(PlateFluorescence, self)._measure(source, **kwargs)
        if source is self._fluorescence:
            self.excite()

    def measureFluoresence(self, excite_time=None, excite_intensity=None):
        self.excite(excite_time, excite_intensity)
        v = self.readTimer()
        self._emission_time.set(v)
        self._emission_time.commit()
        return self._emission_time.get()

    def excite(self, t=None, intensity=None):
        """Will start exciting the well by illuminating the well with excitation LED"""
        result = False
        if t is None:
            t = self.getExcitationTime()
        if intensity is None:
            intensity = self.getExcitationIntensity()
        if self.hasPlate() and self.getPlate().isConnected():
            led = self.findExcitationLED()
            if led is not None:
                result = led.setDACIntensity(intensity)
                # if success; we do the sleeping
                if result and t > 0:
                    time.sleep(t)
                # to be sure, we always turn the LED of
                result = led.setDACIntensity(0)
        return result

    def readTimer(self):
        """Will read the """
        result = 0.0
        if self.hasPlate() and self.getPlate().isConnected():
            connection = self.getPlate().getConnection()
            sensor = self.getEmissionSensor()
            if sensor is not None:
                v = None
                while v is None or v == 65536:
                    v = connection.readRelaxTimer(sensor)
                    if v is not None:
                        result += v
        return result


class PlateFluorescenceException(plateInstrument.PlateInstrumentException):
    """Exception raised by a PlateDiode Instrument"""

    pass
