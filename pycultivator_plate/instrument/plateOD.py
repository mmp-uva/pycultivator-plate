"""
The PlateOD Module provides classes for working with the OD Sensor in a 24 well plate
"""

import plateInstrument, time
from pycultivator.instrument import InstrumentVariable

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PlateOD(plateInstrument.PlateInstrument):
    """The PlateOD models a ODSensor in the 24 well plate incubator"""

    INSTRUMENT_TYPE_SYNONYMS = {
        plateInstrument.PlateInstrument.INSTRUMENT_TYPE_OD
    }

    namespace = "plate.instrument.od"
    default_settings = {
        "path.length": 1,               # path length of the light through the sample
        "flash.intensity": 200,         # intensity of LED in uE
        "replicates": 3,                # number of readings to take
        "replicates.background": None,  # number of readings to take for signal
        "replicates.signal": None,      # number of readings to take for signal
        "time.wait": 0.01,              # number of seconds to wait before reading absorption
    }

    _od = InstrumentVariable(None, private=True)
    """Calibrated OD Value"""
    _od_raw = InstrumentVariable(None, private=True)
    """Raw OD Value"""
    _od_background = InstrumentVariable(None, private=True)
    """Amount of light in uE without signal"""
    _od_flash = InstrumentVariable(None, private=True)
    """Actual Amount of light in uE provided"""
    _od_signal = InstrumentVariable(None, private=True)
    """Amount of light in uE with signal"""

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PlateOD, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)
        self._diode = None
        self._led = None

    # Getters / Setters

    def getLEDWavelength(self):
        """Returns the wavelength (in nanometers) at which this instrument measures the OD

        Essentially a wrapper to the wavelength property of the OD LED

        :return: Wavelength at which this Instrument measures
        :rtype: None or int
        """
        result = None
        if self.hasLED():
            result = self.getLED().wavelength
        return result

    def getPathLength(self):
        """Return the path length of the light through the sample"""
        return self.getSetting("path.length")

    def getFlashIntensity(self):
        return self.getSetting("flash.intensity")

    def setFlashIntensity(self, value):
        """Set the intensity of the LED Flash """
        self.setSetting("flash.intensity", value)
        return self.getFlashIntensity()

    def getReplicates(self):
        """Returns the number of readings to take and average

        :return: Number of readings to take
        :rtype: int
        """
        result = self.getSetting("replicates")
        if not isinstance(result, int):
            result = 1
        return result

    def getSignalReplicates(self):
        result = self.getSetting("replicates.signal")
        if not isinstance(result, int):
            result = self.getReplicates()
        return result

    def getBackgroundReplicates(self):
        result = self.getSetting("replicates.background")
        if not isinstance(result, int):
            result = self.getReplicates()
        return result

    def getTimeWait(self):
        """Returns the amount of time to wait after turning on the OD LED and reading the OD Photodiode

        :return: The amount of time to wait between turning the OD LED on and reading the OD Photodiode
        :rtype: float | int
        """
        return self.getSetting("time.wait")

    def setTimeWait(self, t):
        """Sets the time period between starting the OD LED and reading the OD PhotoDiode

        :param t: The amount of time the device should wait
        :type t: int | float
        :return: The amount of time the device waits (now after setting the value)
        :rtype: int | float
        """
        self.setSetting("time.wait", t)
        return self.getTimeWait()

    def getDiode(self, force=False):
        """Return the Diode used by this OD Sensor

        :rtype: pycultivator_plate.instrument.plateDiode.PlateDiode
        """
        if force or self._diode is None:
            if self.hasParent():
                self._diode = self.getWell().getPhotoDiode()
        return self._diode

    def hasDiode(self):
        return self.getDiode() is not None

    def getLED(self, force=False):
        """Return the LED used by this OD Sensor

        :rtype: pycultivator_plate.instrument.plateODLED.PlateODLED
        """
        if force or self._led is None:
            if self.hasParent():
                self._led = self.getWell().getODLED()
        return self._led

    def hasLED(self):
        return self.getLED() is not None

    def setLED(self, led):
        self._led = led

    def getODRawValue(self):
        """The raw OD Value"""
        return self._od_raw.get()

    @property
    def od_raw(self):
        return self.getODRawValue()

    def getODValue(self):
        """The calibrated OD Value"""
        value = self._od_raw.get()
        od = self.calibrate(value)
        if od < 0:
            od = 0
        self._od.set(od)
        return self._od.get()

    @property
    def od_value(self):
        return self.getODValue()

    def getODBackground(self):
        """The amount of light measured by the photodiode without signal in uE"""
        return self._od_background.get()

    @property
    def od_background(self):
        return self.getODBackground()

    def getODFlash(self):
        """The actual amount light provided by the LED in uE"""
        return self._od_flash.get()

    @property
    def od_flash(self):
        return self.getODFlash()

    def getODSignal(self):
        """The amount of light measured by the photodiode with signal in uE"""
        return self._od_signal.get()

    @property
    def od_signal(self):
        return self.getODSignal()

    # Behaviour methods

    def _measure(self, source, **kwargs):
        super(PlateOD, self)._measure(source, **kwargs)
        if source in (self._od, self._od_signal, self._od_raw, self._od_background, self._od_flash):
            self.measureOD(**kwargs)

    def measureOD(self, **kwargs):
        """Measure the OD in the well

        :param kwargs: Extra keyword arguments passed to readOD
        :return: Dictionary with OD measurement details
        :rtype: dict[str, float]
        """
        # collect replicate measurements
        result = self.readOD(**kwargs)
        if result.get("od_raw") is not None:
            self._od_raw.set(result.get("od_raw"))
            self._od_raw.commit()
            # obtain calibrated value as result
            od_value = self.getODValue()
            result["od_value"] = od_value
            self._od_background.set(result.get("background_intensity"))
            self._od.commit()
            self._od_flash.set(result.get("flash_intensity"))
            self._od_flash.commit()
            self._od_signal.set(result.get("signal_intensity"))
            self._od_signal.commit()
        return result

    def readOD(self, background_replicates=None, signal_replicates=None, aggregate=None, flash=None,
               length=None, wait=None, use_dac=False, use_voltage=False
    ):
        """Measure the OD by flashing with the OD LED and reading the signal using the photo diode.

        :param replicates: Number of replicate photo diode readings
        :param aggregate: How to aggregate the replicate readings of the photo diode in a single value.
        :param flash: Intensity of the flash (in uE/m2/s, or DAC if use_dac is True)
        :param wait: Wait for given number of seconds before measuring signal
        :param use_dac: Set the flash intensity in DAC values
        :param use_voltage: Measure Intensity in Volts (assumes OD LED is calibrated to voltage)
        :return: Tuple with OD, background and flash intensity and signal intensity
        :rtype: dict[str, float]
        """
        od_raw = None
        if flash is None:
            flash = self.getFlashIntensity()
        if length is None:
            length = self.getPathLength()
        if wait is None:
            wait = self.getTimeWait()
        if background_replicates is None:
            background_replicates = self.getBackgroundReplicates()
        if signal_replicates is None:
            signal_replicates = self.getSignalReplicates()
        # turn LED off
        self.applyFlash(0, use_dac=True)
        v_bgr, i_bgr = self.readSignal(background_replicates, aggregate=aggregate)
        # turn LED on
        dac_0, i_0 = self.applyFlash(flash, use_dac=use_dac)    # None = use default intensity = enable light
        if wait > 0:
            time.sleep(wait)
        # read diode
        v_1, i_1 = self.readSignal(signal_replicates, aggregate=aggregate)
        if None not in (v_bgr, dac_0, v_1, i_bgr, i_0, i_1):
            # calculate OD: A = -log10(I/I_0)
            if use_voltage:
                transmittance = (v_1 - v_bgr) / i_0
            else:
                transmittance = (i_1 - i_bgr) / i_0
            import math
            if transmittance > 0:
                od_raw = -math.log10(transmittance) / float(length)
            else:
                od_raw = 0.0
        # turn LED off
        self.applyFlash(0, use_dac=True)
        # report result
        result = {
            'od_raw': od_raw,
            'path_length': length,
            "background_voltage": v_bgr, "background_intensity": i_bgr,
            'flash_dac': dac_0, 'flash_intensity': i_0,
            'signal_voltage': v_1, 'signal_intensity': i_1
        }
        return result

    def readSignal(self, replicates=None, aggregate=None):
        """Reads the amount of light observed by the photodiode.

        :param replicates: Number of replicate to collect
        :type replicates: int | None
        :param aggregate: Method to aggregate the replicate measurements (median or mean)
        :type aggregate: str
        :return: The average intensity of the flash in voltage and uE/m2/s
        :rtype: tuple[float, float]
        """
        if aggregate is None:
            aggregate = "mean"
        result = [None, None]
        if replicates is None:
            replicates = self.getReplicates()
        if not self.hasDiode():
            raise PlateODException("Unable to read PhotoDiode: none defined")
        import numpy as np
        voltages, intensities = np.full(replicates, np.nan), np.full(replicates, np.nan)
        for replicate in range(replicates):
            voltage = self.getDiode().getRawIntensity(True)
            if voltage is not None:
                voltages[replicate] = voltage
            intensity = self.getDiode().calibrate(voltage)
            if intensity is not None:
                intensities[replicate] = intensity
        if len(voltages) > 0:
            if aggregate == "mean":
                result[0] = voltages.mean()
            if aggregate == "median":
                result[0] = np.median(voltages)
        if len(intensities) > 0:
            if aggregate == "mean":
                result[1] = intensities.mean()
            if aggregate == "median":
                result[1] = np.median(intensities)
        return result

    def applyFlash(self, intensity=None, use_dac=False):
        """Set the intensity of the OD LED

        :param intensity: Intensity of the flash
        :type intensity: float | None
        :param use_dac: Whether the intensity value is in DAC instead of uE/m2/s
        :type use_dac: bool
        :return: The actual intensity of the Flash in DAC and uE/m2/s
        :rtype: tuple[ float, float]
        """
        if intensity is None:
            intensity = self.getFlashIntensity()
        if not self.hasLED():
            raise PlateODException("Unable to configure LED: none defined")
        # set LED
        if not use_dac:
            self.getLED().setIntensity(intensity, update=True)
            self.getLED().setState(intensity > 0, update=True)
        else:
            self.getLED().setRawIntensity(intensity, update=True)
            self.getLED().setState(intensity > 0, update=True)
        # return actual intensity
        dac = self.getLED().getDACIntensity(False)
        intensity = self.getLED().getIntensity(False)
        return dac, intensity


class PlateODException(plateInstrument.PlateInstrumentException):
    """Exception raised by a PlateOD Instrument"""

    pass
