
from pycultivator.contrib.console import commands, arguments, exceptions
from pycultivator.contrib.console.commands.device import DeviceCommand
from pycultivator.contrib.console.exceptions.device import *
from pycultivator_plate.plate import Plate


class PlateCommand(DeviceCommand):
    """Generic plate command"""

    namespace = "plate"

    def execute(self):
        if not isinstance(self.device, Plate):
            raise InvalidDeviceException(self, self.device, Plate)
        return super(PlateCommand, self).execute()


class ReadDiodeCommand(PlateCommand):
    """Read the value of one or more photodiode(s) on the plate"""

    name = "read diode"
    command = "read diode"
    arguments = [
        arguments.StringArgument("type", choices=["diode", "well"]),  # read a well or diode
        arguments.IndexRangeArgument("index", takes_all=True),
        arguments.IntegerArgument("replicates", is_required=False, default=1),
    ]
    """:type: set[T <= pycultivator.console.commands.CommandArgument]"""
    description = "Reads the intensity measured with the PhotoDiode"

    def execute(self):
        if not self.device.isConnected():
            name = self.get("device")
            raise NotConnectedException(self, name)
        type_ = self.get("type")
        headers = None
        if type_.lower() == "well":
            headers = ["well", "value"]
        if type_.lower() == "diode":
            headers = ["diode", "value"]
        values = self.collect()
        # print results
        if headers:
            self.console.print_table(values, headers)
        return True

    def collect(self):
        indexes = self.get("index", expand=False)
        """:type: pycultivator.console.arguments.IndexRangeArgument"""
        type_ = self.get("type")
        replicates = self.get("replicates")
        values = []
        if type_.lower() == 'well':
            items = indexes.select(self.device.getWells().keys(), indexes.value)
            if len(items) == 0:
                self.info("No wells matched to given selection!")
            for well_idx in items:
                well = self.device.getWell(well_idx)
                value = well.getPhotoDiode().read_diode_intensity(replicates)
                values.append({'well': well_idx, 'value': value})
        if type_.lower() == 'diode':
            # get diode
            items = indexes.select(range(48), indexes.value)
            """:type: list[int]"""
            if len(items) == 0:
                self.info("No diodes matched to given selection!")
            for diode in items:
                value = self.device.connection.readPhotoDiode(diode)
                values.append({'diode': diode, 'value': value})
        return values


class ConfigureLEDCommand(PlateCommand):

    name = "configure led"
    command = "set led"
    arguments = [
        arguments.StringArgument("type", choices=['diode', 'well']),  # read a well or diode
        arguments.IndexRangeArgument("index", takes_all=True),
        arguments.FloatArgument("intensity"),
        arguments.StringArgument("unit", choices=['dac', 'ue']),
        arguments.BooleanArgument("apply", is_required=False, default=True),
    ]

    def execute(self):
        # read selected unit type
        type_ = self.get("type")
        headers = None
        if type_.lower() == "well":
            headers = ["well", "value"]
        elif type_.lower() == "diode":
            headers = ["diode", "value"]
        # configure intensity
        values = self.configure()
        if self.get("apply"):
            if not self.device.isConnected():
                raise NotConnectedException(self, self.device)
        return True

    def configure(self):
        indexes = self.get("index", expand=False)
        """:type: pycultivator.console.arguments.IndexRangeArgument"""
        type_ = self.get("type")

        values = []
        if type_.lower() == 'well':
            items = indexes.select(self.device.getWells().keys(), indexes.value)
            if len(items) == 0:
                self.info("No wells matched to given selection!")
            for well_idx in items:
                well = self.device.getWell(well_idx)
                """:type: pycultivator_plate.plate.PlateWell"""
                value = well.getPhotoDiode()
                values.append({'well': well_idx, 'value': value})
        if type_.lower() == 'led':
            # get diode
            items = indexes.select(range(48), indexes.value)
            """:type: list[int]"""
            if len(items) == 0:
                self.info("No diodes matched to given selection!")
            for diode in items:
                value = self.device.connection.readPhotoDiode(diode)
                values.append({'diode': diode, 'value': value})
        return values


class ConfigureODLEDCommand(PlateCommand):

    name = "configure odled"
    command = "set odled"
    arguments = [
        arguments.StringArgument("type"),  # read a well or diode
        arguments.IndexRangeArgument("leds", takes_all=True),
        arguments.FloatArgument("intensity"),
        arguments.StringArgument("unit"),
        arguments.BooleanArgument("apply", is_required=False, default=True),
    ]

    def process(self, line):
        result = super(ConfigureODLEDCommand, self).process(line)
        if self.get("apply"):
            if not self.device.isConnected():
                self.info("Unable to apply: not connected")
            else:
                pass
        return result


class ApplyIntensitiesCommand(PlateCommand):

    name = "apply intensities"
    command = "apply intensities"
    arguments = [
        arguments.StringArgument("selection", is_required=False, default="all"),
    ]

    def process(self, line):
        result = super(ApplyIntensitiesCommand, self).process(line)
        selection = self.get("selection").lower()
        for well in self.device.getWells().values():
            for led in well.getLEDs().values():
                if selection == "all" or (selection == "new" and led.has_changed):
                    led.commit()
        return result


class LoadIntensitiesCommand(PlateCommand):

    name = "load intensities"
    command = "load intensities"
    arguments = [
        arguments.BooleanArgument("apply", is_required=False, default=True),
    ]
    description = "Load a CSV with intensity settings into the device"

    def execute(self):
        name = self.get("device")
        fp = self.get("location")
        if fp is not None:
            rows, fields = self.read_csv_file(fp, ["well", "color", "intensity"])
            if len(rows) > 0:
                result = self.load_csv_intensities(self.device, rows, fields)
        if self.get("apply", False):
            if not self.device.isConnected():
                raise NotConnectedException(self, name)
            # apply
        return True

    def load_csv_intensities(self, device, rows, fields):
        result = True
        if "well" in fields and "color" in fields:
            for row in rows:
                well_idx = device.parseToInteger(row['well'], None)
                intensity = device.parseToFloat(row["intensity"], None)
                if intensity is None:
                    self.info("Unable to interpret intensity value: {}".format(row["intensity"]))
                if None not in (well_idx, intensity) and device.hasWell(well_idx):
                    well = device.getWell(well_idx)
                    success = self.apply_well_intensity(
                        well, row["color"], intensity, unit=row.get("unit", None)
                    )
                    if success:
                        self.console.info("Applied {} {} of {} light to well {}".format(
                            row["intensity"], row.get("unit", "uE/m2/s"), row["color"], well_idx
                        ))
                    result = success and result
        elif "led" in fields:
            for row in rows:
                led_idx = device.parseToInteger(row["led"], None)
                intensity = device.parseToFloat(row["intensity"], None)
                if intensity is None:
                    self.info("Unable to interpret intensity value: {}".format(row["intensity"]))
                if None not in (led_idx, intensity) and device.hasInstrument(led_idx):
                    led = device.getInstrument(led_idx)
                    success = self.apply_led_intensity(
                        led, intensity, unit=row.get("unit", None)
                    )
                    if success:
                        self.console.info("Set LED {} to {} {}".format(
                            row["led"], row["intensity"], row.get("unit", "uE/m2/s")
                        ))
                    result = success and result
        return result

    def apply_well_intensity(self, well, color, intensity, unit=None):
        """

        :param well:
        :type well: pycultivator_plate.plate.PlateWell
        :param intensity:
        :type intensity:
        :param color:
        :type color:
        :param unit:
        :type unit:
        :return:
        :rtype:
        """
        leds = well.getLEDsOfColor(color)
        result = len(leds) > 0
        for led in leds.values():
            result = self.apply_led_intensity(led, intensity, unit) and result
        return result

    def apply_led_intensity(self, led, intensity, unit=None, commit=False):
        """

        :param led:
        :type led: pycultivator_plate.instrument.plateLED.PlateLED
        :param intensity:
        :type intensity:
        :param unit:
        :type unit: str
        :return:
        :rtype:
        """
        result = False
        if unit is None:
            unit = "ue"
        if unit.lower() == "ue":
            real = led.calibrate(led.calibrate_inverse(intensity))
            if real != intensity:
                self.console.log("Unable to set LED {} to {} uE/ms/s, instead set to {}".format(
                    led.name, intensity, real
                ))
            result = led.setIntensity(intensity, commit)
        if unit.lower() == "dac":
            result = led.setDACIntensity(intensity, commit)
        if unit.lower() == "raw":
            result = led.setRawIntensity(intensity, commit)
        return result

    def commit_led_intensities(self, device):
        """

        :param device:
        :type device: pycultivator_plate.plate.Plate
        :return:
        :rtype:
        """
        result = False
        if device.isConnected():
            result = True
            for well in device.getWells().values():
                for led in well.getLEDs().values():
                    result = led.commit()
        return result


class SaveIntensitiesCommand(PlateCommand):
    """Saves all intensities"""

    name = "save intensities"
    command = "save intensities"
    arguments = [
        arguments.StringArgument("unit", is_required=False, default="ue"),
    ]

    def execute(self):
        fp = self.get("location")
        unit = self.get("unit")
        self.save_plate_intensities(self.device, fp, unit=unit)

        return True

    def save_plate_intensities(self, device, location, unit=None):
        if unit is None:
            unit = "ue"
        result = False
        rows = []
        fields = ['well', 'led', 'color', 'intensity', 'unit']
        for well in device.getWells().values():
            for led in well.getLEDs().values():
                intensity = self.get_led_intensity(led, unit)
                rows.append({
                    'well': well.index, 'led': led.identity,
                    'color': led.getColorName(),
                    'intensity': intensity, 'unit': unit
                })
        if len(rows) > 0:
            self.write_csv_file(location, rows, fields)
        return result

    def get_led_intensity(self, led, unit=None, update=False):
        """

        :param led:
        :type led: pycultivator_plate.instrument.plateLED.PlateLED
        :param unit:
        :type unit:
        :return:
        :rtype:
        """
        result = None
        if unit is None:
            unit = "ue"
        if unit.lower() == "ue":
            result = led.getIntensity(update=update)
        if unit.lower() == "dac":
            result = led.getDACIntensity(update=update)
        if unit.lower() == "raw":
            result = led.getRawIntensity(update=update)
        return result


def load_commands():
    """Return a list of command classes"""
    return [
        # ReadDiodeCommand,
        # ApplyIntensitiesCommand,
        # LoadIntensitiesCommand,
        # SaveIntensitiesCommand
    ]
