# coding=utf-8
"""Test cases for the packet module"""

from pycultivator.connection.tests import test_SerialConnection
from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator_plate.connection import plateConnection
import serial

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestAddressConversion(UvaSubjectTestCase):
    """Tests the conversion of address to bdc/bd tuples, using Bruno's implementation"""

    _subject_cls = plateConnection.PlateConnection
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the test case

        :return:
        :rtype: pycultivator_plate.connection.plateConnection.PlateConnection
        """
        return super(TestAddressConversion, cls).getSubjectClass()

    def getSubject(self):
        """ Returns the instance of the subject of the test case

        :return:
        :rtype: pycultivator_plate.connection.plateConnection.PlateConnection
        """
        return super(TestAddressConversion, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return object()

    @staticmethod
    def led_to_bdc(lednr):
        board = lednr / 64
        device = lednr / 8 - board * 8
        chan = lednr % 8
        return board, device, chan

    def test_ledTobdc(self):
        for led in range(192):
            self.assertEqual(self.getSubjectClass().ledTobdc(led), self.led_to_bdc(led))

    @staticmethod
    def led_to_bd(lednr):
        board = lednr / 64
        device = 64
        return board, device

    def test_ledTobd(self):
        for led in range(192):
            self.assertEqual(self.getSubjectClass().ledTobd(led), self.led_to_bd(led))

    @staticmethod
    def bdc_to_led(board, device, chan):
        return board * 64 + device * 8 + chan

    def test_bdcToled(self):
        for board in range(3):
            for device in range(8):
                for chan in range(8):
                    self.assertEqual(
                        self.getSubjectClass().bdcToled(board, device, chan),
                        self.bdc_to_led(board, device, chan)
                    )

    @staticmethod
    def ODled_to_bdc(lednr):
        board = lednr / 16
        device = (lednr / 8 - board * 2) + 8
        chan = lednr % 8
        return board, device, chan

    def test_ODTobdc(self):
        for od in range(48):
            self.assertEqual(self.getSubjectClass().ODTobdc(od), self.ODled_to_bdc(od))

    @staticmethod
    def ODled_to_bd(lednr):
        board = lednr / 16
        device = lednr % 16
        return board, device

    def test_ODTobd(self):
        for od in range(48):
            self.assertEqual(self.getSubjectClass().ODTobd(od), self.ODled_to_bd(od))

    @staticmethod
    def sensor_to_bad(sensor):
        board = sensor / 16
        adc = sensor / 8 - board * 2
        diode = sensor % 8
        return board, adc, diode

    def test_sensorTobad(self):
        for idx in range(47):
            self.assertEqual(self.getSubjectClass().sensorTobad(idx), self.sensor_to_bad(idx))

    @staticmethod
    def devchan_to_ODled(board, device, chan):
        return board * 16 + (device - 8) * 8 + chan

    def test_bdcToOD(self):
        for board in range(3):
            for device in [8,9]:
                for chan in range(8):
                    self.assertEqual(
                        self.getSubjectClass().bdcToOD(board, device, chan),
                        self.devchan_to_ODled(board, device, chan)
                    )


class TestPlateConnection(test_SerialConnection.TestSerialConnection):
    """Test case for the Plate Connection class"""

    _subject_cls = plateConnection.PlateConnection
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the test case

        :return:
        :rtype: pycultivator_plate.connection.PlateConnection.PlateConnection
        """
        return super(TestPlateConnection, cls).getSubjectClass()

    def getSubject(self):
        """Returns the subject of the test case

        :return:
        :rtype: pycultivator_plate.connection.PlateConnection.PlateConnection
        """
        return super(TestPlateConnection, self).getSubject()

    def test_getPlate(self):
        with self.assertRaises(plateConnection.PlateException):
            self.getSubject().getBoard(1)
        self.getSubject()._createBoards()
        self.assertIsInstance(self.getSubject().getBoard(1), serial.Serial)

    def test_createPlates(self):
        self.getSubject()._createBoards()
        self.assertEquals(self.getSubject().countBoards(), 3)
        for idx in range(self.getSubject().countBoards()):
            self.assertEquals(self.getSubject()._isOpen[idx], False)
            self.assertIsInstance(self.getSubject().getBoard(idx), serial.Serial)

    def test_getDevice(self):
        self.getSubject()._createDevice()
        self.assertEquals(self.getSubject().countBoards(), 3)
        self.assertIsNone(self.getSubject().getDevice())
        self.getSubject()._setDevice(self.getSubject().getBoards(0))
        self.assertIsInstance(self.getSubject().getDevice(), serial.Serial)
        self.assertEqual(self.getSubject().getDevice(), self.getSubject().getBoards()[0])

    def test_getPort(self):
        self.getSubject()._createDevice()
        self.getSubject()._setDevice(self.getSubject().getBoard(0))
        self.assertEqual(self.getSubject().getPort(), self.getSubject().getBoard(0).port)

    def test_configure(self):
        pass

    def test_configurePlate(self):
        pass

    def test_connect(self):
        pass

    def test_isConnected(self):
        self.assertFalse(self.getSubject().isConnected())
        self.assertTrue(self.getSubject().connect())
        self.assertTrue(self.getSubject().isConnected())
        self.assertTrue(self.getSubject().disconnect())
        self.assertFalse(self.getSubject().isConnected())
        self.assertTrue(self.getSubject().connect())
        self.assertTrue(self.getSubject().isConnected())

    def test_isPlateConnected(self):
        pass

    def test_disconnect(self):
        port = self.subject.getPort()
        self.assertFalse(self.subject.isConnected())
        self.assertTrue(self.getSubject().connect())
        self.assertTrue(self.getSubject().isConnected())
        self.assertTrue(self.getSubject().disconnect())
        self.assertEqual(
            self.subject.getPort(),
            port
        )


class TestFakePlateConnection(TestPlateConnection):
    """Test case for the FAKE Plate Connection class"""

    _subject_cls = plateConnection.PlateConnection
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the test case

        :return:
        :rtype: pycultivator_plate.connection.PlateConnection.FakePlateConnection
        """
        return super(TestPlateConnection, cls).getSubjectClass()

    def getSubject(self):
        """Returns the subject of the test case

        :return:
        :rtype: pycultivator_plate.connection.PlateConnection.FakePlateConnection
        """
        return super(TestPlateConnection, self).getSubject()
