# coding=utf-8
"""Test cases for the packet module"""
from pycultivator.connection import template
from pycultivator.connection.tests.test_Packet import BasePacketTest
from pycultivator_plate.connection import plateTemplate
import struct


class TestPlateTemplate(BasePacketTest):

    _subject_cls = plateTemplate.PlateTemplate
    _abstract = False

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_plate.connection.PlatePacket.PlateTemplate
        """
        return super(TestPlateTemplate, self).getSubject()

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: pycultivator_plate.connection.PlatePacket.PlateTemplate
        """
        return super(TestPlateTemplate, cls).getSubjectClass()

    def test_read24Bit(self):
        f = lambda x: self.getSubjectClass().read24Bit(*x)
        self.assertEqual(f([0, 0, 0]), 0)
        self.assertEqual(f([0, 0, 1]), 1)
        self.assertEqual(f([0, 0, 255]), 255)
        self.assertEqual(f([0, 255, 0]), 255 << 8)
        self.assertEqual(f([127, 0, 0]), 127 << 16)
        self.assertEqual(f([255, 255, 254]), -2)
        self.assertEqual(f([255, 255, 255]), -1)

    def test_toSigned(self):
        f = lambda x, y: self.getSubjectClass().toSigned(x, y)
        # test 0 and 1
        self.assertEqual(f(0, 8), 0)
        self.assertEqual(f(0, 24), 0)
        self.assertEqual(f(1, 8), 1)
        self.assertEqual(f(0, 24), 0)
        # test midrange
        self.assertEqual(f(126, 8), 126)
        self.assertEqual(f((1 << 23) - 2, 24), (1 << 23) - 2)
        self.assertEqual(f(127, 8), 127)
        self.assertEqual(f((1 << 23) - 1, 24), (1 << 23) - 1)
        self.assertEqual(f(128, 8), -128)
        self.assertEqual(f((1 << 23) + 0, 24), -1 * (1 << 23) + 0)
        self.assertEqual(f(129, 8), -127)
        self.assertEqual(f((1 << 23) + 1, 24), -1 * (1 << 23) + 1)
        self.assertEqual(f(130, 8), -126)
        self.assertEqual(f((1 << 23) + 2, 24), -1 * (1 << 23) + 2)
        # test -2 and -1
        self.assertEqual(f(254, 8), -2)
        self.assertEqual(f((1 << 24) - 2, 24), -2)
        self.assertEqual(f(255, 8), -1)
        self.assertEqual(f((1 << 24) - 1, 24), -1)


class TestPhotoDiodeTemplate(TestPlateTemplate):

    _subject_cls = plateTemplate.PhotoDiodeTemplate
    _abstract = False

    def test_getFormats(self):
        self.assertEqual(
            self.getSubject().getFormats(),
            ["B", "B", "B", "c"]
        )
        self.assertEqual(
            self.getSubject().getTemplateFormat(),
            ">BBBc"
        )

    def test_check(self):
        f = lambda x, y: bytearray(struct.pack(x, *y))
        g = lambda x, y: self.getSubject().check(f(x, y))
        self.assertTrue(g(">BBBc", [1, 2, 3, "*"]))
        self.assertFalse(g(">BBB", [1, 2, 3]))
        self.assertFalse(g(">BBBc", [1, 2, 3, "x"]))

    def test_read(self):
        f = lambda x, y: bytearray(struct.pack(x, *y))
        g = lambda x, y: self.getSubject().read(f(x, y))
        self.assertTrue(g(">BBBc", [0, 0, 0, "*"]))
        self.assertTrue(g(">BBBc", [0, 0, 255, "*"]))
        with self.assertRaises(template.TemplateException):
            g(">BBB", [0, 0, 255])
        self.assertEqual(self.getSubject().data, 255)
        self.assertTrue(g(">BBBc", [255, 255, 255, "*"]))
        self.assertEqual(self.getSubject().data, -1)
        self.assertTrue(g(">BBBc", [128, 0, 0, "*"]))
        self.assertEqual(self.getSubject().data, -1 * (1 << 23))


class TestTimeRelaxTemplate(TestPlateTemplate):

    _subject_cls = plateTemplate.TimeRelaxTemplate
    _abstract = False

    def test_getFormats(self):
        self.assertEqual(
            self.getSubject().getFormats(),
            ["B", "B", "c"]
        )
        self.assertEqual(
            self.getSubject().getTemplateFormat(),
            ">BBc"
        )

    def test_check(self):
        f = lambda x, y: bytearray(struct.pack(x, *y))
        g = lambda x, y: self.getSubject().check(f(x, y))
        self.assertTrue(g(">BBc", [1, 2, "*"]))
        self.assertFalse(g(">BB", [1, 2]))
        self.assertFalse(g(">BBc", [1, 2, "x"]))

    def test_read(self):
        f = lambda x, y: bytearray(struct.pack(x, *y))
        g = lambda x, y: self.getSubject().read(f(x, y))
        self.assertTrue(g(">BBc", [0, 0, "*"]))
        self.assertTrue(g(">BBc", [0, 255, "*"]))
        with self.assertRaises(template.TemplateException):
            g(">BB", [0, 255])
        self.assertEqual(self.getSubject().data, 255)
        self.assertTrue(g(">BBc", [255, 255, "*"]))
        self.assertEqual(self.getSubject().data, 255 + (255 << 8))
        self.assertTrue(g(">BBc", [255, 0, "*"]))
        self.assertEqual(self.getSubject().data, 255 << 8)
