# coding=utf-8
"""Test cases for the packet module"""
from pycultivator.connection.tests.test_Packet import BasePacketTest
from pycultivator_plate.connection import platePacket
import struct

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


def createByteArray(header=0x55, device=0, cmd=0, value=0, channel=0):
    result = bytearray()
    result.extend(struct.pack("B", header))
    result.extend(struct.pack("B", device))
    result.extend(struct.pack(">H", value | channel << 12))
    result.extend(struct.pack("B", cmd))
    return result


class TestPlatePacket(BasePacketTest):

    _subject_cls = platePacket.PlatePacket
    _abstract = False

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_plate.connection.PlatePacket.PlatePacket
        """
        return super(TestPlatePacket, self).getSubject()

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: pycultivator_plate.connection.PlatePacket.PlatePacket
        """
        return super(TestPlatePacket, cls).getSubjectClass()

    def test_write24Bit(self):
        f = lambda x: self.getSubjectClass().write24bit(x)
        self.assertEqual(f(0), bytearray('\x00\x00\x00'))
        self.assertEqual(f(1), bytearray('\x00\x00\x01'))
        self.assertEqual(f(255), bytearray('\x00\x00\xff'))
        self.assertEqual(f(255 << 8), bytearray('\x00\xff\x00'))
        self.assertEqual(f((255 << 16)-(1 << 23)), bytearray('\x7f\x00\x00'))
        self.assertEqual(f(-2), bytearray('\xff\xff\xfe'))
        self.assertEqual(f(-1), bytearray('\xff\xff\xff'))

    def test_toUnSigned(self):
        f = lambda x, y: self.getSubjectClass().toUnSigned(x, y)
        # test 0 and 1
        self.assertEqual(f(0, 8), 0)
        self.assertEqual(f(0, 24), 0)
        self.assertEqual(f(1, 8), 1)
        self.assertEqual(f(0, 24), 0)
        # test midrange
        self.assertEqual(f(126, 8), 126)
        self.assertEqual(f((1 << 23) - 2, 24), (1 << 23) - 2)
        self.assertEqual(f(127, 8), 127)
        self.assertEqual(f((1 << 23) - 1, 24), (1 << 23) - 1)
        self.assertEqual(f(-128, 8), 128)
        self.assertEqual(f(-1 * (1 << 23) + 0, 24), (1 << 23) + 0)
        self.assertEqual(f(-127, 8), 129)
        self.assertEqual(f(-1 * (1 << 23) + 1, 24), (1 << 23) + 1)
        self.assertEqual(f(-126, 8), 130)
        self.assertEqual(f(-1 * (1 << 23) + 2, 24), (1 << 23) + 2)
        # test -2 and -1
        self.assertEqual(f(-2, 8), 254)
        self.assertEqual(f(-2, 24), (1 << 24) - 2)
        self.assertEqual(f(-1, 8), 255)
        self.assertEqual(f(-1, 24), (1 << 24) - 1)


class TestPlateCommandPacket(TestPlatePacket):

    _subject_cls = platePacket.PlateCommandPacket
    _abstract = False

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_plate.connection.PlatePacket.PlateCommandPacket
        """
        return super(TestPlatePacket, self).getSubject()

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: type[pycultivator_plate.connection.PlatePacket.PlateCommandPacket]
        """
        return super(TestPlatePacket, cls).getSubjectClass()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(0,1,2,3)

    def test_init(self):
        f = lambda x,y,z,w: self.getSubjectClass()(x,y,z,w)
        self.assertIsInstance(f(0,1,2,3), platePacket.PlateCommandPacket)
        self.assertEqual(f(0,1,2,3).device, 0)
        self.assertEqual(f(0,1,2,3).cmd, 1)
        self.assertEqual(f(0,1,2,3).channel, 2)
        self.assertEqual(f(0,1,2,3).value, 3)

    def test_constrains(self):
        g = lambda: self.getSubjectClass()
        f = lambda x, y, z, w: g()(x, y, z, w)
        # check capacity of device
        self.assertIsInstance(f(0, 1, 2, 3), g())
        self.assertIsInstance(f(255, 1, 2, 3), g())
        with self.assertRaises(ValueError):
            f(256, 1, 2, 3)
        with self.assertRaises(ValueError):
            f(-1, 1, 2, 3)
        # check capacity of cmd
        self.assertIsInstance(f(0, 0, 2, 3), g())
        self.assertIsInstance(f(0, 255, 2, 3), g())
        with self.assertRaises(ValueError):
            f(0, 256, 2, 3)
        with self.assertRaises(ValueError):
            f(0, -1, 2, 3)
        # check capacity of channel
        self.assertIsInstance(f(0, 1, 0, 3), g())
        self.assertIsInstance(f(0, 1, 15, 3), g())
        with self.assertRaises(ValueError):
            f(0, 1, 16, 3)
        with self.assertRaises(ValueError):
            f(0, 1, -1, 3)
        # check capacity of value
        self.assertIsInstance(f(0, 1, 2, 0), g())
        self.assertIsInstance(f(0, 1, 2, 4095), g())
        with self.assertRaises(ValueError):
            f(0, 1, 2, 4096)
        with self.assertRaises(ValueError):
            f(0, 1, 2, -1)

    def test_getFormats(self):
        self.assertEqual(
            self.getSubject().getFormats(),
            ["c", ">B", ">H", ">B"]
        )
        self.assertEqual(
            self.getSubject().getPacketFormat(),
            "c>B>H>B"
        )

    def test_getDataValues(self):
        f = lambda x,y,z,w: self.getSubjectClass()(x,y,z,w).getDataValues()
        self.assertEqual(f(0,1,2,3), [0, 3 | (2 << 12), 1])


class TestLEDVoltageCommand(TestPlateCommandPacket):

    _subject_cls = platePacket.LEDVoltageCommand
    _abstract = False

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(0, 1)

    def test_init(self):
        g = lambda : self.getSubjectClass()
        f = lambda x, y: g()(x, y)
        self.assertIsInstance(self.getSubject(), g())
        s = f(0,1)
        self.assertEqual(s.device, 0)
        self.assertEqual(s.value, 1)

    def test_getDataValues(self):
        f = lambda x, y: self.getSubjectClass()(x, y).getDataValues()
        self.assertEqual(f(0, 1), [0, 1, 0x01])
        self.assertEqual(f(0, 5), [0, 5, 0x01])

    def test_constrains(self):
        g = lambda: self.getSubjectClass()
        f = lambda x, y: g()(x, y)
        # check capacity of device
        self.assertIsInstance(f(0, 1), g())
        self.assertIsInstance(f(63, 1), g())
        with self.assertRaises(ValueError):
            f(64, 1)
        with self.assertRaises(ValueError):
            f(-1, 1)
        # check capacity of value
        self.assertIsInstance(f(0, 0), g())
        self.assertIsInstance(f(0, 1023), g())
        with self.assertRaises(ValueError):
            f(0, 1024)
        with self.assertRaises(ValueError):
            f(0, -1)


class TestLEDDACCommand(TestPlateCommandPacket):

    _subject_cls = platePacket.LEDDACCommand
    _abstract = False

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(0, 1, 0)

    def test_init(self):
        g = lambda : self.getSubjectClass()
        f = lambda x, y, z: g()(x, y, z)
        self.assertIsInstance(self.getSubject(), g())
        s = f(0, 1, 1)
        self.assertEqual(s.device, 0)
        self.assertEqual(s.channel, 1)
        self.assertEqual(s.value, 1)

    def test_getDataValues(self):
        f = lambda x, y, z: self.getSubjectClass()(x, y, z).getDataValues()
        self.assertEqual(f(0, 1, 1), [0, 1 | (1 << 12), 0x02])
        self.assertEqual(f(0, 5, 4094), [0, 4094 | (5 << 12), 0x02])

    def test_constrains(self):
        g = lambda: self.getSubjectClass()
        f = lambda x, y, z: g()(x, y, z)
        # check capacity of device
        self.assertIsInstance(f(0, 1, 0), g())
        self.assertIsInstance(f(63, 1, 0), g())
        with self.assertRaises(ValueError):
            f(64, 1, 0)
        with self.assertRaises(ValueError):
            f(-1, 1, 0)
        # check capacity of value
        self.assertIsInstance(f(0, 1, 0), g())
        self.assertIsInstance(f(0, 1, 4094), g())
        with self.assertRaises(ValueError):
            f(0, 1, 4095)
        with self.assertRaises(ValueError):
            f(0, 1, -1)


class TestLEDStateCommand(TestPlateCommandPacket):

    _subject_cls = platePacket.LEDStateCommand
    _abstract = False

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(0, 1)

    def test_init(self):
        g = lambda : self.getSubjectClass()
        f = lambda x, y: g()(x, y)
        self.assertIsInstance(self.getSubject(), g())
        s = f(0, 1)
        self.assertEqual(s.device, 0)
        self.assertEqual(s.value, 0)
        s = f(0, 0)
        self.assertEqual(s.device, 0)
        self.assertEqual(s.value, 1)

    def test_getDataValues(self):
        f = lambda x, y: self.getSubjectClass()(x, y).getDataValues()
        self.assertEqual(f(0, 0), [0, 1, 0x04])
        self.assertEqual(f(0, 1), [0, 0, 0x04])
        self.assertEqual(f(0, 5), [0, 0, 0x04])

    def test_constrains(self):
        g = lambda: self.getSubjectClass()
        f = lambda x, y: g()(x, y)
        # check capacity of device
        self.assertIsInstance(f(0, 1), g())
        self.assertIsInstance(f(15, 1), g())
        with self.assertRaises(ValueError):
            f(16, 1)
        with self.assertRaises(ValueError):
            f(-1, 1)


class TestPhotoDiodeCommand(TestPlateCommandPacket):

    _subject_cls = platePacket.PhotoDiodeCommand
    _abstract = False

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(0, 1)

    def test_init(self):
        g = lambda: self.getSubjectClass()
        f = lambda x, y: g()(x, y)
        self.assertIsInstance(self.getSubject(), g())
        s = f(0, 1)
        self.assertEqual(s.device, 0)
        self.assertEqual(s.value, (1 << 8) + (1 + 4))

    def test_getDataValues(self):
        f = lambda x, y: self.getSubjectClass()(x, y).getDataValues()
        self.assertEqual(f(0, 1), [0, (1 << 8) + (1 + 4), 8])
        self.assertEqual(f(0, 2), [0, (2 << 8) + (2 + 4), 8])

    def test_constrains(self):
        g = lambda: self.getSubjectClass()
        f = lambda x, y: g()(x, y)
        # check capacity of device
        self.assertIsInstance(f(0, 1), g())
        self.assertIsInstance(f(7, 1), g())
        with self.assertRaises(ValueError):
            f(8, 1)
        with self.assertRaises(ValueError):
            f(-1, 1)
        # check capacity of value
        self.assertIsInstance(f(0, 0), g())
        self.assertIsInstance(f(0, 2), g())
        with self.assertRaises(ValueError):
            f(0, 3)
        with self.assertRaises(ValueError):
            f(0, -1)


class TestTimeRelaxPacket(TestPlateCommandPacket):

    _subject_cls = platePacket.TimeRelaxPacket
    _abstract = False

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(0)

    def test_init(self):
        g = lambda: self.getSubjectClass()
        f = lambda x: g()(x)
        self.assertIsInstance(self.getSubject(), g())
        s = f(0)
        self.assertEqual(s.device, 0)

    def test_getDataValues(self):
        f = lambda x: self.getSubjectClass()(x).getDataValues()
        self.assertEqual(f(0), [0, 0, 0x10])
        self.assertEqual(f(14), [14, 0, 0x10])

    def test_constrains(self):
        g = lambda: self.getSubjectClass()
        f = lambda x: g()(x)
        # check capacity of device
        self.assertIsInstance(f(0), g())
        self.assertIsInstance(f(14), g())
        with self.assertRaises(ValueError):
            f(15)
        with self.assertRaises(ValueError):
            f(-1)
