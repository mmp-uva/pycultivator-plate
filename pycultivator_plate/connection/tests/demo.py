"""A small demo script for quickly testing the connection"""

from pycultivator_plate.connection import plateConnection
from pycultivator_lab.scripts import script
import argparse
import time

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class DemoScript(script.Script):

    LED_COLOR_WHITE = 0
    LED_COLOR_GREEN = 1
    LED_COLOR_RED = 2
    LED_COLOR_BLUE = 3

    LED_COLOR_NAME_MAP = {
        "blue": LED_COLOR_BLUE,
        "green": LED_COLOR_GREEN,
        "red": LED_COLOR_RED,
        "white": LED_COLOR_WHITE,
    }

    LED_COLOR_INDEX_MAP = {
        LED_COLOR_BLUE: "blue",
        LED_COLOR_GREEN: "green",
        LED_COLOR_RED: "red",
        LED_COLOR_WHITE: "white",
    }

    def __init__(self):
        super(DemoScript, self).__init__()
        self._cultivator = None
        self._ports = ["/dev/tty.usbmodem241421", "/dev/tty.usbmodem241431", "/dev/tty.usbmodem2414411"]
        self._fake = False

    def getPorts(self):
        return self._ports

    def setPorts(self, port):
        self._ports = port

    def hasPorts(self):
        return None not in self.getPorts()

    def fakesConnection(self):
        return self._fake is True

    def fakeConnection(self, state):
        self._fake = state is True

    def getConnection(self):
        """ Return the device used by this script

        :return:
        :rtype: pycultivator_plate.connection.PlateConnection.PlateConnection
        """
        return self._cultivator

    def hasConnection(self):
        return self.getConnection() is not None

    def loadConnection(self):
        if self.fakesConnection():
            self.getLog().info("Simulate connection to 24-Plate")
            result = plateConnection.FakePlateConnection()
        else:
            result = plateConnection.PlateConnection()
        self._cultivator = result
        return result

    def clear(self):
        return True

    def _prepare(self):
        result = super(DemoScript, self)._prepare()
        if not self.hasConnection():
            self.loadConnection()
        if self.hasPorts():
            self.getConnection().connect(self.getPorts())
        result = self.getConnection().isConnected() and result
        self.getLog().info("Connected to 24-plate: {}".format(result))
        return result

    def execute(self):
        # blank
        for led in range(192):
            self.apply_led(led, 0)
        # first row 0 to 6: blue
        # self.getLog().info("Set 0-5 to blue")
        for well in range(6):
            address = self.well_color_to_led(well, "blue")
            self.apply_led(address)
        self.getLog().info("Set 6-11 to yellow")
        for well in range(6, 12):
            address = self.well_color_to_led(well, "red")
            self.apply_led(address)
        self.getLog().info("Set 12-18 to green")
        for well in range(12, 18):
            address = self.well_color_to_led(well, "green")
            self.apply_led(address)
        self.getLog().info("Set 6-11 to yellow")
        for well in range(18, 24):
            address = self.well_color_to_led(well, "white")
            self.apply_led(address)
        for well in range(24,48):
            colors = ["blue", "red", "green", "white"]
            for color in colors:
                address = self.well_color_to_led(well, color)
                self.apply_led(address)
        return True

    def well_color_to_led(self, well, color):
        """ Determines the correct address of the LED responsible for

        :param well:
        :type well:
        :param color:
        :type color: int or str
        :return:
        :rtype:
        """
        if isinstance(color, str):
            color = self.LED_COLOR_NAME_MAP[color]
        address = 2 + (color / 2) * 4 + color % 2 + (well % 2) * -2 + (well / 2) * 8
        return address

    def apply_led(self, led_idx, intensity=1000):
        """Set the intensity of a """
        self.getConnection().setLEDIntensity(led_idx, intensity)

    def _clean(self):
        if self.getConnection().isConnected():
            self.getConnection().disconnect()
        result = not self.getConnection().isConnected()
        self.getLog().info("Disconnect from 24-plate: {}".format(result))
        # clean rest up after we disconnect
        result = super(DemoScript, self)._clean() and result
        return result

    def default_arguments(self):
        defaults = super(DemoScript, self).default_arguments()
        defaults["ports"] = self.getPorts()
        defaults["fake"] = self.fakesConnection()
        return defaults

    def define_arguments(self, parser=None):
        if parser is None:
            parser = argparse.ArgumentParser(description="Demo application of Reglo Pump")
        parser = super(DemoScript, self).define_arguments(parser)
        parser.add_argument(
            "--ports", nargs=3, help="Set the ports to which the 24-plate is connected"
        )
        parser.add_argument(
            "-s", action="store_true", dest="fake",
            help="Fake the connection"
        )
        return parser

    def parse_arguments(self, arguments):
        super(DemoScript, self).parse_arguments(arguments)
        ports = arguments.get("ports", self.getPorts())
        if ports is not None:
            self.setPorts(ports)
        self.fakeConnection(arguments.get("fake", False))

if __name__ == "__main__":
    ds = DemoScript()
    # create argument parser
    p = ds.define_arguments()
    # load defaults
    d = ds.default_arguments()
    # parse arguments and overwrite defaults
    d.update(vars(p.parse_args()))
    # read arguments into script
    ds.parse_arguments(d)
    # run script
    ds.run()
