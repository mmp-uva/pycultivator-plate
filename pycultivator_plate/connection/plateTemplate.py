"""Module implementing Template Classes used when communicating with UvA's 24-well plate incubator"""

from pycultivator.connection import template

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PlateTemplate(template.Template):
    """Class capable of reading packets received from the 24-Plate Incubator"""

    @classmethod
    def read24Bit(cls, high, mid, low):
        """Interprets a big-endian (unsigned) 24 bit sequence (divided into 3 groups of 8 bits)

        :param high: First byte (left most)
        :param mid: Middle 8 bits
        :param low: Last byte (right most)
        :return: Signed integer
        :rtype: int
        """
        number = (high << 16) + (mid << 8) + low
        return cls.toSigned(number)

    @classmethod
    def toSigned(cls, number, bitLength=24):
        """Interprets an unsigned integer into a signed integer using two's complement.

        >>> PlateTemplate.toSigned(0, 8)
        0
        >>> PlateTemplate.toSigned(1, 8)
        1
        >>> PlateTemplate.toSigned(126, 8)
        126
        >>> PlateTemplate.toSigned(127, 8)
        127
        >>> PlateTemplate.toSigned(128, 8)
        -128
        >>> PlateTemplate.toSigned(129, 8)
        -127
        >>> PlateTemplate.toSigned(130, 8)
        -126

        >>> PlateTemplate.toSigned(254, 8)
        -2
        >>> PlateTemplate.toSigned(255, 8)
        -1

        :param number: Unsigned number to convert
        :type number: int
        :param bitLength: Number of bits available to represent the number
        :type bitLength: int
        :return: Signed interpretation of unsigned integer
        :rtype: int
        """
        mask = (1 << bitLength) - 1
        if number & (1 << (bitLength - 1)):
            result = number | ~mask
        else:
            result = number & mask
        return result

    def getTemplateFormat(self):
        return "".join([">"] + self.getFormats())


class AcknowledgementTemplate(template.Template):

    def getFormats(self):
        return ["c"]

    def isOk(self):
        return not self.isEmpty() and self.data[0] == "*"


class PhotoDiodeTemplate(PlateTemplate):
    """Interprets the response of the photo diode on a photodiode packet"""

    def clear(self):
        self._data = None

    def getFormats(self):
        return ["B", "B", "B", "c"]

    def check(self, data):
        result = super(PhotoDiodeTemplate, self).check(data)
        if result:
            ack = self.readChar(data, offset=-1)[0]
            result = ack == "*"
        return result

    def isEmpty(self):
        return self.data is None

    def readData(self, data, offset=0):
        hibyte, offset = self.readValue(data, fmt="B", offset=offset)
        mibyte, offset = self.readValue(data, fmt="B", offset=offset)
        lobyte, offset = self.readValue(data, fmt="B", offset=offset)
        # read value
        value = self.read24Bit(hibyte, mibyte, lobyte)
        self.getLog().debug("Received: {} ({}). Parsed to {}".format(
            (hibyte << 16) + (mibyte << 8) + lobyte,
            self.byteArrayToBit([hibyte, mibyte, lobyte]),
            value
        ))
        # TODO: convert to voltage
        return value, offset


class TimeRelaxTemplate(PlateTemplate):
    """Interprets the response on the TimeRelax packet"""

    def clear(self):
        self._data = None

    def getFormats(self):
        return ["B", "B", "c"]

    def check(self, data):
        result = super(TimeRelaxTemplate, self).check(data)
        if result:
            ack = self.readChar(data, offset=-1)[0]
            result = ack == "*"
        return result

    def isEmpty(self):
        return self.data is None

    def readData(self, data, offset=0):
        hibyte, offset = self.readValue(data, fmt="B", offset=offset)
        lobyte, offset = self.readValue(data, fmt="B", offset=offset)
        # read value
        value = (hibyte << 8) + lobyte
        return value, offset


class PlateTemplateException(template.TemplateException):
    """Exception raised by a PlateTemplate class"""

    def __init__(self, msg):
        super(PlateTemplateException, self).__init__(msg)
