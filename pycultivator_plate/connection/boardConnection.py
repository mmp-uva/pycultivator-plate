"""Implement communication with one 24 plate board"""

from pycultivator.connection.serialConnection import *
from pycultivator.core.pcParser import assert_range
import platePacket, plateTemplate
import serial

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ = [
    "BoardConnection", "FakeBoardConnection", "BoardException"
]


class BoardConnection(SerialConnection):
    """A driver class implementing API for UvA 24-well plate incubator

    24 well plate incubator uses 3 independent driver boards, each connected by it's own serial connection to control
    two 24-well plates.
    """

    namespace = "plate.board.connection"
    default_settings = {
            "port": None,
            "rate": 9600,
            "timeout": 5,
        }

    USB_VENDOR_ID = int(0x29dd)
    USB_PRODUCT_ID = int(0x8001)

    # Conversion methods
    @classmethod
    def ledTodc(cls, ledNr):
        """Converts a LED Number into a tuple with device and channel number

        Used when working with DAC value of a LED
        dcToled(d, c) == led and d,c == ledTodc(led)

        :param ledNr: The LED Number to convert (0-64)
        :type ledNr: int
        :return: Tuple with Board (0-2), Device (0-7) and Channel (0-7) numbers
        :rtype: tuple[int, int]
        """
        assert_range(ledNr, lwr=0, upr=64, name="ledNr", include=True)
        # 0-7, 64-71, 128-135: device == 8 // ... // 56-63, 120-127, 184-191: device == 7
        device = (ledNr % 64) / 8
        # 0-7: 0-7 // 8-15: 0-7 // 16-24: 0-7 // ... etc
        chan = ledNr % 8
        return device, chan

    @classmethod
    def ledTod(cls, ledNr):
        """Converts a LED Number into a tuple with board and device number.

        Used when working with LED states

        :param ledNr: The LED Number to convert (0-64)
        :type ledNr: int
        :return: device number
        :rtype: int
        """
        assert_range(ledNr, lwr=0, upr=64, name="ledNr", include=True)
        return 64

    @classmethod
    def dcToled(cls, device, channel):
        """Converts a Device and Channel tuple into a LED Number

        Used to translate BDC Numbers into LED Numbers.
        dcToled(d, c) == led and d,c == ledTodc(led)

        :param device: Device number (0-7)
        :type device: int
        :param channel: Channel number (0-7)
        :type channel: int
        :return: LED Number (0-64)
        :rtype: int
        """
        assert_range(device, lwr=0, upr=7, name="device", include=True)
        assert_range(channel, lwr=0, upr=7, name="channel", include=True)
        return device * 8 + channel

    @classmethod
    def ODTodc(cls, ledNr):
        """Converts a OD LED Number into a Device, Channel tuple

        To make OD measurements

        ODTobdc(led) == b,d,c and bdcToOD(b,d,c) == led

        :param ledNr: OD Diode Number  (0-16)
        :type ledNr: int
        :return: Tuple with Device (8-9) and Channel (0-7) number
        :rtype: tuple[int, int, int]
        """
        assert_range(ledNr, lwr=0, upr=15, name="number", include=True)
        # 0-7,16-23,32-39 : device == 8 // 8-15,24-31,40-47: device == 9
        device = ledNr / 8 + 8
        # 0-7: channel == 0-7 // 8-15: channel == 0-7 // ... etc
        chan = ledNr % 8
        return device, chan

    @classmethod
    def ODTod(cls, ledNr):
        """Converts an OD LED Number into a device number

        Used to quickly turn the LED on or off.

        :param ledNr: OD LED Number (0-16)
        :type ledNr: int
        :return: Tuple with Board (0-2) and Device (0-16)
        :rtype: tuple[int, int]
        """
        assert_range(ledNr, lwr=0, upr=15, name="ledNr", include=True)
        return ledNr % 16

    @classmethod
    def sensorToad(cls, sensor):
        """Converts a Sensor Number into a Board, ADC and Diode tuple

        Board: which driver board to use (0-2)
        ADC: which Analog to Digital Converter to use (0-1)
        Diode: which diode on the ADC to read (0-7)

        :param sensor: Number of OD Sensor (i.e. well number, 0-47)
        :type sensor: int
        :return: Tuple with ADC (0-1) and Diode (0-7)
        :rtype: tuple[int, int, int]
        """
        assert_range(sensor, lwr=0, upr=15, name="sensor", include=True)
        # 0-7,16-23,32-39 : adc == 0 // 8-15,24-31,40-47: adc == 1
        adc = sensor // 8
        # 0-7: diode == 0-7 // 8-15: diode == 0-7 // ... etc
        diode = sensor % 8
        return adc, diode

    @classmethod
    def dcToOD(cls, device, channel):
        """Converts a Board, Device, Channel number into a OD LED Number (0-

        dcToOD(d,c) == led and ODTodc(led) == d,c

        :param device: Number of the device (8-9)
        :type device: int
        :param channel: Number of the channel (0-7)
        :type channel: int
        :return: OD LED Number (0-15)
        :rtype: int
        """
        assert_range(device, lwr=8, upr=9, name="device", include=True)
        assert_range(channel, lwr=0, upr=7, name="channel", include=True)
        return (device - 8) * 8 + channel

    def __init__(self, settings=None, **kwargs):
        super(BoardConnection, self).__init__(settings=settings, **kwargs)
        self._retry_max = 2
        self._seqNr = 0

    @classmethod
    def getFake(cls):
        return FakeBoardConnection

    # Getters / Setters

    def getDevice(self):
        """The low-level Serial Connection to the Board

        :rtype: serial.Serial
        """
        return self._device

    def _setDevice(self, device):
        """Set the low-level Serial Connection to the device"""
        if device is not None and not isinstance(device, serial.Serial):
            raise BoardException("Received invalid plate object (only None or serial.Serial allowed)")
        self._device = device
        return self.getDevice()

    def _createDevice(self):
        self._setDevice(serial.Serial())
        return self.getDevice()

    # Connection methods

    def listPorts(self, vendor_id=False, product_id=False, validate=False):
        """List all ports that may connect to a 24-well Plate Board

        :param validate: Validate these ports to make sure they are of a 24-Well Plate
        :return: List of port names
        :rtype: list[str]
        """
        import serial.tools.list_ports
        ports = serial.tools.list_ports.comports()
        if vendor_id is None:
            vendor_id = self.USB_VENDOR_ID
        if vendor_id is not False:
            ports = filter(lambda p: p.vid == vendor_id, ports)
        if product_id is None:
            product_id = self.USB_PRODUCT_ID
        if product_id is not False:
            ports = filter(lambda p: p.pid == product_id, ports)
        # return port names
        results = [str(p.device) for p in ports]
        if validate:
            results = self.validatePorts(ports)
        return results

    def validatePorts(self, ports=None, vendor_id=None, product_id=None):
        """Check a list of possible port names"""
        results = []
        if ports is None:
            ports = self.listPorts(vendor_id=vendor_id, product_id=product_id, validate=False)
        # iterate over list
        for port in ports:
            if self.validatePort(port):
                results.append(port)
            # remove tested ports
            for p in port:
                ports.remove(p)
        return results

    def validatePort(self, port):
        """Check a set of 3 ports"""
        result = False
        old_port = self.getPort()
        if self.isConnected():
            self.getLog().warning("There is still an open connection, closing it!")
            self.disconnect()
        try:
            if self.connect(port) and self.readPhotoDiode(0, 0) is not None:
                result = True
        except Exception as e:
            pass
        finally:
            self.disconnect()
            self._setPort(old_port)
        return result

    def find(self, save_first=True, vendor_id=None, product_id=None):
        """Find Plate and update configuration"""
        # find and validate
        ports = self.validatePorts(vendor_id=vendor_id, product_id=product_id)
        if len(ports) > 1 and save_first:
            port = ports[1]
            self._setPort(port)
        return ports

    def _configure(self, settings=None, **kwargs):
        """Configures all the plates"""
        if self.isConnected():
            raise BoardException("Unable to configure an open port")
        if not self.hasDevice():
            self._createDevice()
        if not self.hasDevice():
            raise BoardException("Unable to configure, no connection object")
        if settings is None:
            settings = {}
        # now merge/override settings with internal settings
        self.settings.update(self.settings.reduce_dict(settings, self.getNameSpace()))
        # now merge/override with kwargs
        self.settings.update(kwargs)
        # apply settings to device
        self.getDevice().port = self.getPort()
        self.getDevice().baudrate = self.getBaudRate()
        self.getDevice().timeout = self.getTimeout()
        self.getDevice().writeTimeout = self.getWriteTimeout()
        self.getDevice().bytesize = self.getByteSize()
        self.getDevice().parity = self.getParity()
        self.getDevice().stopbits = self.getStopBits()
        self.getDevice().rtscts = self.getRTSCTS()
        self.getDevice().dsrdtr = self.getDSRDTR()
        self.getDevice().xonxoff = self.getXonXoff()
        return self

    def _connect(self, port=None):
        """Connects all plates"""
        # if port is set, store it as setting
        if port is not None:
            self._setPort(port)
        # check if a port was set at all
        if self.getPort() is None:
            raise BoardException("No port set, cannot open connection.")
        # check if board is already connected, disconnect
        if self.isConnected():
            self.disconnect()
        if not self.hasDevice():
            self._createDevice()
        try:
            # first configure the port with all the settings
            self._configure()
            # then open
            self.getDevice().open()
            self.getLog().debug("Opened serial connection to {}".format(self.getPort()))
        except OSError as os:
            self.getLog().critical("OSError: {}".format(os))
        except serial.SerialException as se:
            self.getLog().critical("Unable to open port: {}".format(se))
        except ValueError as ve:
            self.getLog().critical("Unable to open port with these options {}".format(ve))
        return self.isConnected()

    def isConnected(self):
        """Returns whether there is at least one plate connected"""
        return self.hasDevice() and self.getDevice().is_open

    def disconnect(self):
        """Closes the connection to all plates

        :return: True if at least one plate was closed
        """
        if self.isConnected():
            self.getDevice().close()
            self.getLog().debug("Closed serial connection to {}".format(self.getPort()))
        return self.getDevice().is_open

    # sending and receiving methods

    def send_receive_ok(self, packet, pause=0, attempts=None):
        """Writes a packets and expects a OK packet as response

        Sets the active device to the given board index before sending and receiving.

        :param packet: The packet to write
        :type packet: pycultivator_plate.connection.platePacket.platePacket
        :param pause: Amount of time to wait between the writing and reading
        :type pause: int
        :param attempts: Amount of times to try reading the data
        :type attempts: int
        :param board: Which driver board to use (index)
        :type board: int
        :rtype: bool
        """
        template = plateTemplate.AcknowledgementTemplate()
        self.send_receive(packet, template=template, pause=pause, attempts=attempts)
        return template.isOk()

    # Controlling methods

    def setLEDIntensity(self, device, channel, intensity):
        """Sets the intensity (in DAC values) of a single LED .

        :param device: Index of the device on the board
        :type device: int
        :param channel: Index of the channel on the board
        :type channel: int
        :param intensity: DAC Value (0-4095)
        :type intensity: int
        :return:
        :rtype: bool
        """
        result = False
        assert_range(device, lwr=0, upr=63, name="device", include=True)
        assert_range(channel, lwr=0, upr=15, name="channel", include=True)
        assert_range(intensity, lwr=0, upr=4094, name="intensity", include=True)
        if self.hasDevice():
            packets = [
                platePacket.LEDDACCommand(device=device, channel=channel, value=intensity),
                platePacket.LEDDACCommand(device=device, channel=10, value=255),
                platePacket.LEDDACCommand(device=device, channel=10, value=0)
            ]
            result = True
            for p in packets:
                success = self.send_receive_ok(packet=p)
                if not success:
                    self.getLog().warning(
                        "Failed to set intensity to {:4.0f} on board {} (d:{:2d}|c:{:2d}".format(
                            intensity, self.getPort(), device, channel
                        )
                    )
                result = success and result
        return result

    def setLEDState(self, device, state):
        """Toggle the state of a group of LEDs at once"""
        result = False
        assert_range(device, lwr=0, upr=64, name="device", include=True)
        if self.hasDevice():
            p = platePacket.LEDStateCommand(device=device, state=state)
            result = self.send_receive_ok(p)
            if not result:
                self.getLog().warning(
                    "Failed set state of LED on board {} (d:{:2d})".format(
                        self.getPort(), device
                    )
                )
        return result

    def readRelaxTimer(self, device):
        """INTERNAL reads the relax time; returns the timer value"""
        result = None
        assert_range(device, lwr=0, upr=63, name="device", include=True)
        if self.hasDevice():
            cmd = platePacket.TimeRelaxPacket(device=device)
            t = plateTemplate.TimeRelaxTemplate()
            self.send_receive(packet=cmd, template=t)
            if t.hasData():
                result = t.data
        return result

    def readPhotoDiode(self, adc, diode):
        result = None
        if self.hasDevice():
            cmd = platePacket.PhotoDiodeCommand(diode=diode, adc=adc)
            res = plateTemplate.PhotoDiodeTemplate()
            self.send_receive(packet=cmd, template=res)
            if res.hasData():
                result = res.data
        return result


class FakeBoardConnection(BoardConnection, FakeSerialConnection):
    """A fake connection to the UvA 24 plate incubator"""

    def __init__(self, settings=None, **kwargs):
        super(FakeBoardConnection, self).__init__(settings=settings, **kwargs)
        self._connected = False

    def isConnected(self):
        return self._connected

    def _connect(self, port=None):
        # if port is set, store it as setting
        if port is not None:
            self._setPort(port)
        # check if a port was set at all
        if self.getPort() is None:
            raise BoardException("No port set, cannot open connection.")
        if not self.hasDevice():
            self._createDevice()
        try:
            # first configure the port with all the settings
            self._configure()
            # then open
            # self.getDevice().open()
            self._connected = True
            self.getLog().debug("Opened serial connection to {}".format(self.getPort()))
        except OSError as os:
            self.getLog().critical("OSError: {}".format(os))
        except serial.SerialException as se:
            self.getLog().critical("Unable to open port: {}".format(se))
        except ValueError as ve:
            self.getLog().critical("Unable to open port with these options {}".format(ve))
        return self.isConnected()

    def disconnect(self):
        result = False
        if self.isConnected():
            # do nothing
            # self.getPlate(index).close()
            self.getLog().debug("Closed serial connection to {}".format(self.getPort()))
            self._connected = False
            # self._setBoardPort(index, None)
            result = not self.isConnected()
        return result

    # controlling methods

    def setLEDIntensity(self, device, channel, intensity):
        return True

    def readRelaxTimer(self, device):
        return 5

    def setLEDState(self, device, state):
        return True

    def readPhotoDiode(self, adc, diode):
        return 1.0


class BoardException(SerialException):
    """Exception raised by Uva24Plate Board Connections"""

    pass
