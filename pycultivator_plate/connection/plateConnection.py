"""The UvA 24 Plate Connection module implements a pyCultivator connection to UvA's 24-well plate incubator"""

from pycultivator.core.pcVariable import ConstrainedVariable
from pycultivator.connection.connection import Connection, FakeConnection, ConnectionException
from boardConnection import BoardConnection, FakeBoardConnection, BoardException
import serial

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ = [
    "PlateConnection", "FakePlateConnection", "PlateException"
]


class PlateConnection(Connection):
    """Facilitate communication with the 24-well plate set-up"""

    namespace = "plate.connection"
    default_settings = {
            "rate": 9600,
            "timeout": 5,
        }

    MAX_PLATE_COUNT = 3
    USB_VENDOR_ID = int(0x29dd)
    USB_PRODUCT_ID = int(0x8001)

    # Conversion methods
    @classmethod
    def ledTobdc(cls, ledNr):
        """Converts a LED Number into a tuple with board, device and channel number

        Used when working with DAC value of a LED
        bdcToled(b, d, c) == led && b,d,c == ledTobdc(led)

        :param ledNr: The LED Number to convert (0-191)
        :type ledNr: int
        :return: Tuple with Board (0-2), Device (0-7) and Channel (0-7) numbers
        :rtype: tuple[int, int, int]
        """
        ConstrainedVariable.validate(ledNr, lwr=0, upr=191, name="ledNr", include=True)
        # 0-63: board == 0 // 64-127: board == 1 // 128-191: board == 2
        board = ledNr / 64
        # 0-7, 64-71, 128-135: device == 8 // ... // 56-63, 120-127, 184-191: device == 7
        device = (ledNr % 64) / 8
        # 0-7: 0-7 // 8-15: 0-7 // 16-24: 0-7 // ... etc
        chan = ledNr % 8
        return board, device, chan

    @classmethod
    def ledTobd(cls, ledNr):
        """Converts a LED Number into a tuple with board and device number.

        Used when working with LED states

        :param ledNr: The LED Number to convert (0-191)
        :type ledNr: int
        :return: Tuple with Board (0-2), Device (0-7) numbers
        :rtype: tuple[int, int]
        """
        ConstrainedVariable.validate(ledNr, lwr=0, upr=191, name="ledNr", include=True)
        # 0-63: board == 0 // 64-127: board == 1 // 128-191: board == 2
        board = ledNr / 64
        device = 64
        return board, device

    @classmethod
    def bdcToled(cls, board, device, channel):
        """Converts a Board, Device, Channel tuple into a LED Number

        Used to translate BDC Numbers into LED Numbers.
        bdcToled(b, d, c) == led && b,d,c == ledTobdc(led)

        :param board: Board number (0-2)
        :type board: int
        :param device: Device number (0-7)
        :type device: int
        :param channel: Channel number (0-7)
        :type channel: int
        :return: LED Number (0-191)
        :rtype: int
        """
        ConstrainedVariable.validate(board, lwr=0, upr=2, name="board", include=True)
        ConstrainedVariable.validate(device, lwr=0, upr=7, name="device", include=True)
        ConstrainedVariable.validate(channel, lwr=0, upr=7, name="channel", include=True)
        return board * 64 + device * 8 + channel

    @classmethod
    def ODTobdc(cls, ledNr):
        """Converts a OD LED Number into a Board, Device, Channel tuple

        To make OD measurements

        ODTobdc(led) == b,d,c and bdcToOD(b,d,c) == led

        :param ledNr: OD Diode Number  (0-48)
        :type ledNr: int
        :return: Tuple with Board (0-2), Device (8-9) and Channel (0-7) number
        :rtype: tuple[int, int, int]
        """
        ConstrainedVariable.validate(ledNr, lwr=0, upr=47, name="number", include=True)
        # 0-15: board == 0 // 16-31: board == 1 // 32-47: board == 2
        board = ledNr / 16
        # 0-7,16-23,32-39 : device == 8 // 8-15,24-31,40-47: device == 9
        device = (ledNr % 16) / 8 + 8
        # 0-7: channel == 0-7 // 8-15: channel == 0-7 // ... etc
        chan = ledNr % 8
        return board, device, chan

    @classmethod
    def ODTobd(cls, ledNr):
        """Converts an OD LED Number into a Board, Device tuple

        Used to quickly turn the LED on or off.

        :param ledNr: OD LED Number (0-48)
        :type ledNr: int
        :return: Tuple with Board (0-2) and Device (0-15)
        :rtype: tuple[int, int]
        """
        ConstrainedVariable.validate(ledNr, lwr=0, upr=48, name="ledNr", include=True)
        # 0-15: board == 0 // 16-31: board == 1 // 32-47: board == 2
        board = ledNr / 16
        # 0-15: device == 0-15 // 16-31: device == 0-15 // 32-47: device == 0-15
        device = ledNr % 16
        return board, device

    @classmethod
    def sensorTobad(cls, sensor):
        """Converts a Sensor Number into a Board, ADC and Diode tuple

        Board: which driver board to use (0-2)
        ADC: which Analog to Digital Converter to use (0-1)
        Diode: which diode on the ADC to read (0-7)

        :param sensor: Number of OD Sensor (i.e. well number, 0-47)
        :type sensor: int
        :return: Tuple with Board (0-2), ADC (0-1) and Diode (0-7)
        :rtype: tuple[int, int, int]
        """
        ConstrainedVariable.validate(sensor, lwr=0, upr=47, name="sensor", include=True)
        # 0-15: board == 0 // 16-31: board == 1 // 32-47: board == 2
        board = sensor / 16
        # 0-7,16-23,32-39 : adc == 0 // 8-15,24-31,40-47: adc == 1
        adc = (sensor % 16) / 8
        # 0-7: diode == 0-7 // 8-15: diode == 0-7 // ... etc
        diode = sensor % 8
        return board, adc, diode

    @classmethod
    def bdcToOD(cls, board, device, channel):
        """Converts a Board, Device, Channel number into a OD LED Number (0-

        bdcToOD(b,d,c) == led and ODTobdc(led) == b,d,c

        :param board: Number of the driver board to use (0-2)
        :type board: int
        :param device: Number of the device (8-9)
        :type device: int
        :param channel: Number of the channel (0-7)
        :type channel: int
        :return: OD LED Number (0-47)
        :rtype: int
        """
        ConstrainedVariable.validate(board, lwr=0, upr=2, name="board", include=True)
        ConstrainedVariable.validate(device, lwr=8, upr=9, name="device", include=True)
        ConstrainedVariable.validate(channel, lwr=0, upr=7, name="channel", include=True)
        return board * 16 + (device - 8) * 8 + channel

    def __init__(self, requires_all=True, settings=None, **kwargs):
        super(PlateConnection, self).__init__(settings=settings, **kwargs)
        # serialConnection.SerialConnection.__init__(self, settings=settings, **kwargs)
        self._requires_all =requires_all
        self._boards = [None, None, None]
        self._retry_max = 2
        self._seqNr = 0

    @classmethod
    def getFake(cls):
        return FakePlateConnection

    # Getters / Setters

    def requiresAll(self):
        return self._requires_all is True

    def requireAll(self, state):
        self._requires_all = state is True

    def getBoards(self):
        """Returns the serial connection to each driver board

        :rtype: list[None or pycultivator_plate.connection.boardConnection.BoardConnection]
        """
        return self._boards

    def countBoards(self):
        """Counts the number of driver boards managed by this connection

        :rtype: int
        """
        return len(self.getBoards())

    def hasBoardIndex(self, index):
        """Returns whether the given board position is known"""
        return -self.countBoards() <= index < self.countBoards()

    def hasBoard(self, index):
        """Returns whether this connection has a driver board (not None) at the given index

        :rtype: bool
        """
        return self.hasBoardIndex(index) and self.getBoards()[index] is not None

    def getBoard(self, index):
        """Returns the plate at the given index

        :rtype: None | pycultivator_plate.connection.boardConnection.BoardConnection
        """
        if not self.hasBoardIndex(index):
            raise PlateException("Invalid index - {} - given".format(index))
        return self.getBoards()[index]

    def _setBoard(self, index, board):
        """Sets the plate at the given index"""
        if not self.hasBoardIndex(index):
            raise PlateException("Invalid index - {} - given".format(index))
        if board is not None and not isinstance(board, BoardConnection):
            raise PlateException("Received invalid plate object (only None or serial.Serial allowed)")
        self.getBoards()[index] = board
        return self.getBoards()

    def _createBoards(self):
        """Creates serial objects for all plates"""
        self._devices = [BoardConnection() for __ in range(3)]
        self._isOpen = [False, False, False]
        return self.getBoards()

    def _createBoard(self, index):
        """Create one serial object for the plate at position `index` """
        if not self.hasBoardIndex(index):
            raise PlateException("Invalid board index - {} - given".format(index))
        self._setBoard(index, BoardConnection())
        return self.getBoards()

    def _setActiveDevice(self, board):
        """Sets a board to be the device that is currently used by the connection

        This makes sure that we can use the read/write implementations of the SerialConnection
        The result after _setActiveDevice: getDevice() == getBoard(board)

        :type board: int
        :rtype: pycultivator_plate.connection.boardConnection.BoardConnection
        """
        if not self.hasBoard(board):
            raise PlateException("Invalid board index - {} - given".format(board))
        self._setDevice(self.getBoard(board))
        return self.getDevice()

    def _createDevice(self):
        return self._createBoards()

    def getBoardPorts(self):
        """Returns the port location of each board

        :rtype: list[None or str]
        """
        results = [None, None, None]
        for idx in range(3):
            if self.hasBoard(idx):
                results[idx] = self.getBoardPort(idx)
        return results

    def getBoardPort(self, index):
        """Returns the port of plate at position `index`"""
        result = None
        if not self.hasBoardIndex(index):
            raise PlateException("Invalid board index - {} - given".format(index))
        if self.hasBoard(index):
            result = self.getBoard(index).getPort()
        return result

    def _setBoardPort(self, index, port):
        """Sets the port of one plate at position `index`"""
        if not self.hasBoardIndex(index):
            raise PlateException("Invalid board index - {} - given".format(index))
        if not self.hasBoard(index):
            self._createBoard(index)
        if self.hasBoard(index):
            board = self.getBoard(index)
            board.reconfigure(port=port, autoOpen=False)
        return self.getBoardPorts()

    def getPort(self):
        return self.getBoardPorts()

    def _setPort(self, port):
        if not isinstance(port, list) or len(port) != 3:
            raise PlateException("Invalid board ports given, need a list of 3 ports")
        for idx, p in enumerate(port):
            self._setBoardPort(idx, p)
        return self.getPort()

    # Connection methods

    def listPorts(self, vendor_id=False, product_id=False, validate=False):
        """List all sets of 3 ports that may connect to a 24-well Plate

        :param validate: Validate these ports to make sure they are of a 24-Well Plate
        :return: List of port names
        :rtype: list[str]
        """
        import serial.tools.list_ports
        ports = serial.tools.list_ports.comports()
        if vendor_id is None:
            vendor_id = self.USB_VENDOR_ID
        if vendor_id is not False:
            ports = filter(lambda p: p.vid == vendor_id, ports)
        if product_id is None:
            product_id = self.USB_PRODUCT_ID
        if product_id is not False:
            ports = filter(lambda p: p.pid == product_id, ports)
        # return port names
        results = [str(p.device) for p in ports]
        if validate:
            results = self.validatePorts(ports)
        return results

    def validatePorts(self, ports=None, vendor_id=None, product_id=None):
        """Check a list of possible port names"""
        results = []
        if ports is None:
            ports = self.listPorts(vendor_id=vendor_id, product_id=product_id, validate=False)
        # sort by device name
        ports = sorted(ports)
        # iterate over list
        while len(ports) >= 3:
            # take first three
            port = ports[0:3]
            # test
            if self.validatePort(port):
                results.append(port)
            # remove tested ports
            for p in port:
                ports.remove(p)
        return results

    def validatePort(self, port):
        """Check a set of 3 ports"""
        result = False
        old_port = self.getPort()
        if len(port) < 3:
            raise ValueError("Need a list or tuple of 3 port names")
        if self.isConnected():
            self.getLog().warning("This connection object is connected to a device, closing it!")
            self.disconnect()
        try:
            if self.connect(port) and self.readPhotoDiode(0) is not None:
                result = True
        except Exception as e:
            pass
        finally:
            self.disconnect()
            self._setPort(old_port)
        return result

    def find(self, save_first=True, vendor_id=None, product_id=None):
        """Find Plate and update configuration"""
        # find and validate
        ports = self.validatePorts(vendor_id=vendor_id, product_id=product_id)
        if len(ports) > 1 and save_first:
            port = ports[1]
            self._setPort(port)
        return ports

    def _configure(self, settings=None, **kwargs):
        """Configures all the plates"""
        if self.isConnected():
            raise PlateException("Unable to configure an open port")
        if not self.hasDevice():
            raise PlateException("Unable to configure, devices not initialized")
        if settings is None:
            settings = {}
        port = self.getPort()
        # now merge/override settings with internal settings
        self.settings.update(self.settings.reduce_dict(settings, self.getNameSpace()))
        # now merge/override with kwargs
        self.settings.update(kwargs)
        if not isinstance(self.getPort(), list) or len(self.getPort()) != 3:
            self.getLog().warning("Invalid port was set, revert back to old port settings")
            self._setPort(port)
        if not isinstance(self.getPort(), list) or len(self.getPort()) != 3:
            raise PlateException("Invalid ports set, need a list of 3 ports")
        for index in range(self.countBoards()):
            board = self.getBoard(index)
            board.reconfigure(self.settings)
        return self

    def connect(self, port=None, require_all=True):
        result = False
        if port is None:
            port = self.getPort()
        if not result and port is not None:
            result = self._connect(port=port, require_all=require_all)
        # Allow for other connection options
        return result

    def _connect(self, port=None, require_all=None):
        """Connects all plates"""
        # return False if no plate was connected
        ports = port
        if not isinstance(ports, (tuple, list)) or len(ports) != 3:
            ports = [None, None, None]
        if None in ports:
            ports = self.getPort()
        for index in range(self.countBoards()):
            port = ports[index]
            self._connectBoard(index, port)
        return self.isConnected(require_all=require_all)

    def _connectBoard(self, index, port=None):
        """Connects one plate"""
        if not self.hasBoardIndex(index):
            raise PlateException("Invalid board index - {} -".format(index))
        # check if board was created, if not create it
        if not self.hasBoard(index):
            self._createBoard(index)
        # load board
        board = self.getBoard(index)
        if board is None:
            raise PlateException("No board loaded")
        try:
            board.connect(port=port)
            self.getLog().debug("Opened serial connection to {}".format(self.getBoardPort(index)))
        except BoardException as be:
            self.getLog().critical(be)
        except OSError as os:
            self.getLog().critical("OSError: {}".format(os))
        except serial.SerialException as se:
            self.getLog().critical("Unable to open port: {}".format(se))
        except ValueError as ve:
            self.getLog().critical("Unable to open port with these options {}".format(ve))
        return self.isBoardConnected(index)

    def isConnected(self, require_all=None):
        """Returns whether there is at least one plate connected"""
        if require_all is None:
            require_all = self.requiresAll()
        results = [self.isBoardConnected(index) for index in range(self.countBoards())]
        if require_all:
            result = False not in results
        else:
            result = True in results
        return result

    def isBoardConnected(self, index):
        """Checks if the driver board (identified with index) is connected"""
        if not self.hasBoardIndex(index):
            raise PlateException("Invalid board index - {} -".format(index))
        board = self.getBoard(index)
        return board is not None and board.isConnected()

    def disconnect(self):
        """Closes the connection to all plates

        :return: True if at least one plate was closed
        """
        if self.isConnected(require_all=False):
            for index in range(self.countBoards()):
                self.disconnectBoard(index)
        return not self.isConnected(require_all=False)

    def disconnectBoard(self, index):
        """Closes the connection to one plate"""
        result = False
        if self.isBoardConnected(index):
            self.getBoard(index).disconnect()
            self.getLog().debug("Closed serial connection to {}".format(self.getBoardPort(index)))
            result = not self.isBoardConnected(index)
        return result

    # Controlling methods

    def setLEDIntensity(self, ledNr, intensity):
        """Sets the intensity (in DAC values) of a single LED .

        :param ledNr: The LED to set (0-191)
        :type ledNr: int
        :param intensity: DAC Value (0-4095)
        :type intensity: int
        :return:
        :rtype: bool
        """
        ConstrainedVariable.validate(ledNr, lwr=0, upr=191, name="ledNr", include=True)
        ConstrainedVariable.validate(intensity, lwr=0, upr=4094, name="intensity", include=True)
        (board, device, channel) = self.ledTobdc(ledNr)
        if not self.hasBoard(board):
            raise PlateException("No board initialized")
        b = self.getBoard(board)
        return b.setLEDIntensity(device, channel, intensity)

    def setLEDState(self, ledNr, state):
        ConstrainedVariable.validate(ledNr, lwr=0, upr=191, name="led")
        (board, device) = self.ledTobd(ledNr)
        if not self.hasBoard(board):
            raise PlateException("No board initialized")
        b = self.getBoard(board)
        return b.setLEDState(device, state)

    def readRelaxTimer(self, sensor):
        """Reads the relax timer off a well

        :param sensor: Sensor number to use (0-15)
        :type sensor: int
        :return: The duration of the signal in seconds
        :rtype: None or int
        """
        ConstrainedVariable.validate(sensor, lwr=0, upr=15, name="sensor", include=True)
        (board, device) = self.sensorTobad(sensor)
        if not self.hasBoard(board):
            raise PlateException("No board initialized")
        b = self.getBoard(board)
        result = b.readRelaxTimer(device)
        if result is None:
            self.getLog().warning(
                "Failed to get the relax time from sensor {} [board: {}, device: {}]".format(
                    sensor, board, device
                )
            )
            result = None
        return result

    def setODIntensity(self, ledNr, intensity):
        """Sets the intensity of the OD LED

        :param sensor: Index fo the sensor (0-48)
        :type sensor: int
        :param intensity: Light Intensity in DAC Values (0-4095)
        :return: Whether the operation succeeded
        """
        ConstrainedVariable.validate(ledNr, lwr=0, upr=47, name="od led", include=True)
        ConstrainedVariable.validate(intensity, lwr=0, upr=4095, name="value", include=True)
        (board, device, channel) = self.ODTobdc(ledNr)
        if not self.hasBoard(board):
            raise PlateException("No board initialized")
        b = self.getBoard(board)
        # pass to normal intensity method
        return b.setLEDIntensity(device=device, channel=channel, intensity=intensity)

    def setODState(self, ledNr, state):
        """ Sets the state of the OD LED

        :param sensor: Index of the sensor (0-48)
        :type sensor: int
        :param state: The state to set the sensor to
        :type state: bool
        :return: Whether the operation succeeded
        :rtype: bool
        """
        ConstrainedVariable.validate(ledNr, lwr=0, upr=47, name="od led", include=True)
        (board, device) = self.ODTobd(ledNr)
        if not self.hasBoard(board):
            raise PlateException("No board initialized")
        b = self.getBoard(board)
        return b.setLEDState(device, state)

    def readPhotoDiode(self, index):
        """Measures the intensity in the well using the photo diode

        :param index: Index of the diode
        :type index: int
        :return: Measured intensity in volt
        :rtype: float
        """
        ConstrainedVariable.validate(index, lwr=0, upr=47, name="Diode", include=True)
        (board, adc, diode) = self.sensorTobad(index)
        if not self.hasBoard(board):
            raise PlateException("No board initialized")
        b = self.getBoard(board)
        result = b.readPhotoDiode(adc, diode)
        if result is None:
            self.getLog().error(
                "Failed read intensity from PhotoDiode {:2} [board: {:1} | adc: {:1} | diode: {:1}]".format(
                    index, board, adc, diode
                )
            )
        return result


class FakePlateConnection(PlateConnection, FakeConnection):
    """A fake connection to the UvA 24 plate incubator"""

    def _createBoards(self):
        """Creates serial objects for all plates"""
        self._devices = [FakeBoardConnection() for __ in range(3)]
        self._isOpen = [False, False, False]
        return self.getBoards()

    def _createBoard(self, index):
        """Create one serial object for the plate at position `index` """
        if not self.hasBoardIndex(index):
            raise PlateException("Invalid board index - {} - given".format(index))
        self._setBoard(index, FakeBoardConnection())
        return self.getBoards()


class PlateException(ConnectionException):
    """Exception raised by Uva24Plate Connections"""

    def __init__(self, msg):
        super(PlateException, self).__init__(msg=msg)
