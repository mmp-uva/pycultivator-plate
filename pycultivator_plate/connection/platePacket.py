"""Module implementing data packets used when communicating with UvA's 24-well plate incubator"""

import struct
from pycultivator.connection import packet
from pycultivator.core.pcParser import assert_range

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class AcknowledgementPacket(packet.Packet):
    """An acknowledgement packet """

    def getFormats(self):
        return ["c"]

    def getValues(self):
        return ["*"]


class PlatePacket(packet.SandwichPacket):
    """Class capable of writing packets that can be understood by the 24-Plate incubator

    General scheme:
    sys.write(chr(self.HEADER))           # HEADER
    sys.write(chr(device))                # Device ID
    sys.write(chr(value & 0xff00) >> 8)   # hibyte
    sys.write(chr(value & 0x00ff))        # lobyte
    sys.write(chr(0x10))                  # cmd

    Data bytes (hi + lo) can also be written as:
    sys.write(struct.pack(">H", value)
    """

    @classmethod
    def write24bit(cls, number):
        """Writes a number as an unsigned big-endian 24-bit sequence.

        :param number: Signed number to write as unsigned 24-bit sequence
        :type number: int
        :return: 24-bit sequence
        :rtype: bytearray
        """
        signed_num = cls.toUnSigned(number)
        high = (signed_num & 0xff0000) >> 16
        mid = (signed_num & 0x00ff00) >> 8
        low = signed_num & 0x0000ff
        return bytearray(struct.pack("3B", high, mid, low))

    @classmethod
    def toUnSigned(cls, number, bitLength=24):
        """Writes a signed integer number to an unsigned integer using two's complement.

        toUnsigned(0, 8) = 0
        toUnsigned(1, 8) = 1

        toUnsigned(126, 8) = 126
        toUnsigned(127, 8) = 127
        toUnsigned(-128, 8) = 128
        toUnsigned(-127, 8) = 129
        toUnsigned(-126, 8) = 130

        toUnsigned(-2, 8) = 254
        toUnsigned(-1, 8) = 255

        :param number: Signed number to convert
        :type number: int
        :param bitLength: Number of bits available to represent the number
        :type bitLength: int
        :return: Unsigned form of signed integer
        :rtype: int
        """
        assert_range(number, -1 * (1 << bitLength - 1), (1 << bitLength - 1) - 1, include=True)
        result = number
        if number < 0:
            result = number & (1 << bitLength) - 1
        return result


class PlateCommandPacket(PlatePacket):
    """"""

    def __init__(self, device, cmd, channel=0, value=0):
        super(PlatePacket, self).__init__()
        # check max capacity device byte
        assert_range(device, 0, 255, name="device", include=True)
        # check max capacity of cmd byte
        assert_range(cmd, 0, 255, name="cmd", include=True)
        # check max capacity of channel (half byte)
        assert_range(channel, 0, 15, name="channel", include=True)
        # check max capacity of value (1.5 byte)
        assert_range(value, 0, 4095, name="value", include=True)
        self.device = device
        self.cmd = cmd
        self.channel = channel
        self.value = value

    def getHeaderFormats(self):
        return ["c"]

    def getHeaderValues(self):
        return ["\x55"]

    def getDataFormats(self):
        return [">B", ">H", ">B"]

    def getDataValues(self):
        return [self.device, int(self.value) | (self.channel << 12), self.cmd]


class LEDVoltageCommand(PlateCommandPacket):

    def __init__(self, device, value):
        assert_range(device, 0, 63, name="device", include=True)  # 0 <= device <= 63
        assert_range(value, 0, 1023, name="value", include=True)  # 0 <= value <= 1023
        super(LEDVoltageCommand, self).__init__(device=device, cmd=0x01, value=value)


class LEDDACCommand(PlateCommandPacket):

    def __init__(self, device, channel, value):
        assert_range(device, 0, 63, name="device", include=True)  # 0 <= device <= 63
        assert_range(value, 0, 4094, name="value", include=True)  # 0 <= value <= 4094
        super(LEDDACCommand, self).__init__(device=device, cmd=0x02, value=value, channel=channel)


class LEDStateCommand(PlateCommandPacket):

    def __init__(self, device, state):
        assert_range(device, 0, 63, name="device", include=True)  # 0 <= device <= 63
        if not isinstance(state, int):
            state = 1 if state else 0
        if isinstance(state, int):
            state = 0 if state > 0 else 1
        super(LEDStateCommand, self).__init__(device=device, cmd=0x04, value=state)


class PhotoDiodeCommand(PlateCommandPacket):

    def __init__(self, diode, adc):
        assert_range(diode, 0, 7, name="device", include=True)  # 0 <= diode <= 7
        assert_range(adc, 0, 2, name="adc", include=True)  # 0 <= adc <= 2
        # make adc value
        value = (adc << 8) + (adc + 4)
        super(PhotoDiodeCommand, self).__init__(device=diode, cmd=0x08, value=value)


class TimeRelaxPacket(PlateCommandPacket):

    def __init__(self, device):
        assert_range(device, 0, 14, name="device", include=True)  # 0 <= device <= 14
        super(TimeRelaxPacket, self).__init__(device=device, cmd=0x10)


class PlatePacketException(packet.PacketException):
    """Exception raised by a latePacket class"""

    def __init__(self, msg):
        super(PlatePacketException, self).__init__(msg)
