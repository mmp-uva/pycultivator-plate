""" The UvA 24Plate module provides classes for modelling a 24-well plate incubator"""

from pycultivator.device import ComplexDevice, ComplexDeviceException, Channel, ChannelException

from pycultivator_plate.connection import PlateConnection
from pycultivator_plate.instrument import plateDiode, plateLED, plateODLED, plateOD, plateFluorescence

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ = [
    "Plate", "PlateException", "PlateWell", "PlateWellException"
]


class Plate(ComplexDevice):
    """ The LightCalibrationPlate device """

    namespace = "plate.device"
    default_settings = {
        # add default settings for this class
    }

    required_connection = PlateConnection

    MAX_CHANNELS = 48

    def __init__(self, identity, parent, settings=None, **kwargs):
        super(Plate, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)

    def getConnection(self):
        """Returns the connection used to connect to the device

        :return: The connection being used.
        :rtype: pycultivator_plate.connection.plateConnection.PlateConnection
        """
        return super(Plate, self).getConnection()

    def createConnection(self, class_=None, fake=False, settings=None, **kwargs):
        """Creates a new connection and uses it for this device"""
        port = None
        if self.hasConnection():
            port = self.getConnection().getPort()
        result = super(Plate, self).createConnection(class_=class_, fake=fake, settings=settings, **kwargs)
        if port is not None:
            result._setPort(port)
        return result

    def getChannels(self):
        """ Returns all the wells known by this device

        :rtype: dict[int, pycultivator_plate.plate.PlateWell]
        """
        return super(Plate, self).getChannels()

    def getWells(self):
        """Return all wells connected to this plate

        :rtype: dict[int, pycultivator_plate.plate.PlateWell]
        """
        return self.getChannels()

    def getChannel(self, idx):
        """ Returns the well at the given index

        :rtype: pycultivator_plate.plate.PlateWell
        """
        return super(Plate, self).getChannel(idx)

    def getWell(self, idx):
        """Returns the well at the given index

        :rtype: pycultivator_plate.plate.PlateWell
        """
        return self.getChannel(idx)

    def hasWell(self, idx):
        """Return whether a well at the given index exists"""
        return self.hasChannel(idx)

    # Behaviour methods

    def connect(self, fake=False):
        """Connect to the Light Calibration Plate"""
        result = False
        if self.hasConnection():
            result = self.getConnection().connect()
        return result


class PlateWell(Channel):
    """A channel in a LightCalibrationPlate"""

    namespace = "device.plate.well"
    default_settings = {
        # add default settings for this class
    }



    # instruments

    INSTRUMENTS = {
        plateLED.PlateLED: 4,
        plateDiode.PlateDiode: 1,
        plateODLED.PlateODLED: 1,
        plateOD.PlateOD: 1,
        plateFluorescence.PlateFluorescence: 1,
    }


    @classmethod
    def instrumentNameToClass(cls, name):
        """Returns the  class reference corresponding to the given name

        :rtype: type[pycultivator_plate.instrument.PlateInstrument.PlateInstrument]
        """
        if name not in cls.INSTRUMENTS.keys():
            raise PlateException("Invalid name. {} - is not know.".format(name))
        return cls.INSTRUMENTS[name]

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PlateWell, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)
        # add fluorescence and od instruments

    # Getters // Setters

    def getLEDs(self):
        """Returns all the LED Instruments connected to this well

        :return:
        :rtype: dict[str|int, pycultivator_plate.instrument.plateLED.PlateLED]
        """
        # t = self.instrumentNameToClass(self.INSTRUMENT_LED)
        return self.getInstrumentsOfClass(plateLED.PlateLED)

    def getLED(self, name, leds=None):
        """Return the led with the given ID

        :rtype: pycultivator_plate.instrument.plateLED.PlateLED
        """
        result = None
        if leds is None:
            leds = self.getLEDs()
        if name in leds.keys():
            result = leds[name]
        return result

    def getLEDColors(self, as_name=False):
        """Returns the indexes of all (unique) colors that can be emitted in this well

        :rtype: list[int]
        """
        if as_name:
            result = self.getLEDColorNames()
        else:
            result = self.getLEDColorIndexes()
        return result

    def getLEDColorIndexes(self):
        """Returns the indexes of all (unique) colors that can be emitted in this well

        :rtype: list[int]
        """
        result = []
        for k, led in self.getLEDs().items():
            color = led.getColor()
            if color not in result:
                result.append(color)
        return result

    def getLEDColorNames(self):
        """Returns the names of all (unique) colors that can be emitted in this well

        :rtype: list[str]
        """
        result = []
        for k, led in self.getLEDs().items():
            color = led.getColorName()
            if color not in result:
                result.append(color)
        return result

    def hasLEDColor(self, color):
        result = False
        if isinstance(color, int):
            result = self.hasLEDColorIndex(color)
        if isinstance(color, str):
            result = self.hasLEDColorName(color)
        return result

    def hasLEDColorIndex(self, color):
        return color in self.getLEDColors()

    def hasLEDColorName(self, color):
        return color in self.getLEDColorNames()

    def getLEDAtIndex(self, index, leds=None):
        """Return the led at the given index

        :param index:
        :type index: int
        :param leds: Dictionary of LEDs to search, if None use LEDS attached to this object
        :type leds: dict[str|int, pycultivator_plate.instrument.PlateLED.PlateLED]
        :return:
        :rtype: dict[str|int, pycultivator_plate.instrument.plateLED.PlateLED]
        """
        result = None
        if leds is None:
            leds = self.getLEDs()
        if len(leds) > index:
            result = leds[index]
        return result

    def getLEDsOfColor(self, color, leds=None):
        """Returns all LEDS that emit the given color

        :param color: The desired color (as index or name)
        :type color: str | int
        :param leds: Dictionary of LEDs to search, if None use LEDS attached to this object
        :type leds: dict[str|int, pycultivator_plate.instrument.plateLED.PlateLED]
        :rtype: dict[str|int,pycultivator_plate.instrument.plateLED.PlateLED]
        """
        results = {}
        if leds is None:
            leds = self.getLEDs()
        for k, led in leds.items():
            if led.emitsColor(color):
                results[k] = led
        return results

    def getLEDsAtAddress(self, address, leds=None):
        """Returns all LEDS that correspond to the given address

        :param address:
        :type address: int
        :param leds: Dictionary of LEDs to search, if None use LEDS attached to this object
        :type leds: dict[str|int, pycultivator_plate.instrument.PlateLED.PlateLED]
        :return:
        :rtype: dict[str|int, pycultivator_plate.instrument.plateLED.PlateLED]
        """
        results = {}
        if leds is None:
            leds = self.getLEDs()
        for k, led in leds.items():
            if led.getAddress() == address:
                results[k] = led
        return results

    def getPhotoDiode(self):
        """Returns the photo diode

        :return:
        :rtype: pycultivator_plate.instrument.plateDiode.PlateDiode
        """
        result = None
        # t = self.instrumentNameToClass(self.INSTRUMENT_PD)
        instruments = self.getInstrumentsOfClass(plateDiode.PlateDiode).values()
        if len(instruments) > 0:
            result = instruments[0]
        return result

    def hasPhotoDiode(self):
        return self.getPhotoDiode() is not None

    def getODLED(self):
        """Returns the OD LED

        :rtype: pycultivator_plate.instrument.plateODLED.PlateODLED
        """
        result = None
        instruments = self.getInstrumentsOfClass(plateODLED.PlateODLED).values()
        if len(instruments) > 0:
            result = instruments[0]
        return result

    def hasODLED(self):
        return self.getODLED() is not None

    def getODInstrument(self):
        """Returns the OD Instrument of this well

        :rtype: None or pycultivator_plate.instrument.plateOD.PlateOD
        """
        result = None
        instruments = self.getInstrumentsOfClass(plateOD.PlateOD).values()
        if len(instruments) > 0:
            result = instruments[0]
        return result

    def hasODInstrument(self):
        return self.getODInstrument() is not None

    def getFluorescenceInstrument(self):
        result = None
        instruments = self.getInstrumentsOfClass(plateFluorescence.PlateFluorescence).values()
        if len(instruments) > 0:
            result = instruments[0]
        return result

    def hasFluorescenceInstrument(self):
        return self.getFluorescenceInstrument() is not None

    def getPlate(self):
        """ Returns the Plate object to which this well belongs.

        :return: The plate object
        :rtype: pycultivator_plate.plate.Plate
        """
        return self.getDevice()

    def getDevice(self):
        """ Returns the Plate object to which this well belongs.

        :return: The plate object
        :rtype: pycultivator_plate.plate.Plate
        """
        return super(PlateWell, self).getDevice()

    def getInstrumentAtAddress(self, address, instruments=None):
        """Returns a dictionary of all instrument with the given address"""
        if instruments is None:
            instruments = self.getInstruments()
        results = {}
        for k, i in instruments.items():
            if i.getAddress() == address:
                results[k] = i
        return results

    # Behaviour methods

    def setLightIntensity(self, value, leds=None):
        """Set the Intensity of all the LEDs in the well (or all LEDs given)

        :type value: Light intensity in uE/m2/s
        :type leds: list[pycultivator_plate.instrument.plateLED.PlateLED] or None
        """
        result = False
        if leds is None:
            leds = self.getLEDs().values()
        for instrument in leds:
            result = instrument.control("intensity", value)
        return result

    def setLightState(self, state, leds=None):
        """Set the State of all LEDs in the well (or all LEDs given)

        :type state: bool
        :type leds: list[pycultivator_plate.instrument.plateLED.PlateLED] or None
        """
        result = False
        if leds is None:
            leds = self.getLEDs().values()
        for instrument in leds:
            result = instrument.control("state", state)
            # if we enable light: just return as boolean, if we disabled light
            result = result if state else isinstance(result, (int, float))
        return result


class PlateException(ComplexDeviceException):
    """An exception raised by the 24-well plate"""

    pass


class PlateWellException(ChannelException):
    """An exception raised by a well in the 24-well plate"""

    pass