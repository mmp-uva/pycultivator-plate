
from julesTk import view
from pycultivator_plate_tools.plate_control.controller.plate import PlateController, PlateView
from datetime import datetime as dt

import os
import sys

try:
    import tkFileDialog
except ImportError:
    import tkinter.fileDialog as tkFileDialog


class MainView(view.View):

    def __init__(self, parent, controller):
        super(MainView, self).__init__(parent, controller)

    @property
    def controller(self):
        """Return controller for this view
        
        :return: Controller instance 
        :rtype: pycultivator_plate_tools.plate_control.controller.main.MainController
        """
        return super(MainView, self).controller

    @property
    def status(self):
        return str(self.get_variable("status").get())

    @status.setter
    def status(self, value):
        self.get_variable("status").set(value)

    def _prepare(self):
        self.pack(fill=view.tk.BOTH, expand=1)
        # create menu
        self._prepare_menu()
        self.configure_row(self, [0])
        self.configure_column(self, [1])
        # create status bar frame
        frms = view.ttk.Frame(self, relief=view.tk.SUNKEN, borderwidth=1)
        self.add_widget("status_frm", frms)
        # frms.pack(side=view.tk.BOTTOM, fill=view.tk.X)
        frms.grid(row=1, column=0, columnspan=2, sticky="ew")
        self._prepare_status(frms)
        # create left navbar frame
        frmn = view.ttk.Frame(self, relief=view.tk.SUNKEN, borderwidth=2)
        self.add_widget("tool_frm", frmn)
        # frmn.pack(side=view.tk.LEFT, fill=view.tk.Y)
        frmn.grid(row=0, column=0, sticky="nsew", padx=1, pady=1)
        self._prepare_tools(frmn)
        # create body frame
        frmb = view.ttk.Frame(self)
        self.add_widget("body_frm", frmb)
        # frmb.pack(side=view.tk.TOP, fill=view.tk.BOTH, expand=1, padx=1)
        frmb.grid(row=0, column=1, sticky="nsew", padx=1)
        self._prepare_body(frmb)

    def _prepare_menu(self):
        # general menu
        menu = view.tk.Menu(self)
        self.add_widget("menu", menu)
        # add file menu
        cmenu = view.tk.Menu(menu, tearoff=0)
        self.add_widget("config_menu", cmenu)
        acc = "Ctrl+o"
        event = "<Control-KeyPress-o>"
        if sys.platform == "darwin":
            event = "<Command-KeyPress-o>"
            acc = "Cmd+o"
        self.application.bind(event, self.open_config)
        cmenu.add_command(label="Open", command=self.open_config, accelerator=acc)
        cmenu.add_command(label="Save", command=self.save_config, state="disabled")
        cmenu.add_separator()
        cmenu.add_command(label="Quit", command=self.stop)
        menu.add_cascade(label="Config", menu=cmenu)
        # add plate menu
        pmenu = view.tk.Menu(menu, tearoff=0)
        self.add_widget("plate_menu", pmenu)
        pmenu.add_command(label="Connect", command=self.plate_connect)
        pmenu.add_command(label="Simulate", command=self.plate_connect_fake)
        pmenu.add_command(label="Disconnect", command=self.plate_disconnect, state="disabled")
        pmenu.add_separator()
        # add menu
        plmenu = view.tk.Menu(pmenu, tearoff=0)
        self.add_widget("plate_light_menu", plmenu)
        plmenu.add_command(label="Load", command=self.plate_light_load)
        plmenu.add_command(label="Save", command=lambda: self.plate_light_save(True))
        plmenu.add_command(label="Save As", command=lambda: self.plate_light_save(False))
        plmenu.add_separator()
        plmenu.add_command(label="Apply", command=self.plate_light_apply)
        pmenu.add_cascade(label="Light", menu=plmenu, state="disabled")
        menu.add_cascade(label="Plate", menu=pmenu, state="disabled")
        # add tools menu
        tmenu = view.tk.Menu(menu, tearoff=0)
        self.add_widget("tools_menu", tmenu)
        tmenu.add_command(label="Console", command=self.console)
        menu.add_cascade(label="Tools", menu=tmenu)
        # set things up
        self.application.config(menu=menu)

    def _prepare_navigation(self, parent=None):
        if parent is None:
            parent = self
        # lbt = view.ttk.Label(parent, text="Navigation")
        # lbt.pack(side=view.tk.TOP, fill=view.tk.X)
        tv = view.ttk.Treeview(parent)
        tv.insert("", 0, text="Home")
        self.add_widget("navigation", tv)
        tv.pack(side=view.tk.TOP, fill=view.tk.BOTH, expand=1)

    def _prepare_tools(self, parent=None):
        if parent is None:
            parent = self
        lbt = view.ttk.Label(parent, text="Actions")
        lbt.pack(side=view.tk.TOP, fill=view.tk.X)
        btco = view.ttk.Button(parent, text="Open config", command=self.open_config)
        self.add_widget("bt_open_config", btco)
        btco.pack(side=view.tk.TOP, fill=view.tk.X)
        btcc = view.ttk.Button(parent, text="Connect", command=self.plate_connect, state="disabled")
        self.add_widget("bt_connect", btcc)
        btcc.pack(side=view.tk.TOP, fill=view.tk.X)
        btcd = view.ttk.Button(parent, text="Disconnect", command=self.plate_disconnect, state="disabled")
        self.add_widget("bt_disconnect", btcd)
        btcd.pack(side=view.tk.TOP, fill=view.tk.X)


    def _prepare_body(self, parent=None):
        if parent is None:
            parent = self
        # create plate 1
        ctrp1 = PlateController(self.controller, None)
        frmp1 = PlateView(parent, ctrp1, 1, relief=view.tk.RAISED, borderwidth=1)
        ctrp1.set_view(frmp1)
        frmp1.pack(side=view.tk.LEFT, fill=view.tk.BOTH, expand=1)
        self.add_widget("plate_1_frm", frmp1)
        # create plate 2
        ctrp2 = PlateController(self.controller, None)
        frmp2 = PlateView(parent, ctrp2, 2, relief=view.tk.RAISED, borderwidth=1)
        ctrp2.set_view(frmp2)
        frmp2.pack(side=view.tk.RIGHT, fill=view.tk.BOTH, expand=1)
        self.add_widget("plate_2_frm", frmp2)

    def _prepare_status(self, parent=None):
        if parent is None:
            parent = self
        v = self.add_variable("status", view.tk.StringVar())
        v.set("Welcome, ...")
        lbs = view.ttk.Label(parent, textvariable=v)
        self.add_widget("status", lbs)
        lbs.pack(side=view.tk.BOTTOM, fill=view.tk.X)

    def show_progress(self, title, message, command=None, mode="indeterminate", auto_close=True):
        from julesTk.utils import progress
        pb = progress.ProgressBar(self, command=command, mode=mode, auto_close=auto_close)
        pb.view.title(title)
        pb.view.message = message
        pb.view.geometry('300x100+200+200')
        pb.view.minsize(300, 100)
        return pb

    def open_config(self, event=None):
        # show dialog
        fp = tkFileDialog.askopenfilename(
            filetypes=[("XML File", "*.xml")],
            initialdir=os.getcwd()
        )
        if fp is not None and fp != "":
            pb = self.show_progress("Loading..", "Loading plate from configuration")

            def f():
                return self.controller.open_config(fp)
            pb.start(f, auto_close=True, block=True)
            if pb.result:
                # provide access to the connection menu
                self.update_config_menu(True)
                self.update_plate_menu(False)

    def save_config(self, event=None):
        pass

    def update_config_menu(self, state):
        v = "normal" if state else "disabled"
        self.get_widget("config_menu").entryconfig("Save", state=v)
        self.get_widget("menu").entryconfig("Plate", state=v)

    def stop(self, event=None):
        self.application.stop()

    def plate_connect(self, event=None):
        pb = self.show_progress("Loading..", "Loading plate from configuration")
        pb.start(self.controller.plate_connect, auto_close=True, block=True)
        if pb.result:
            self.update_plate_menu(True)

    def plate_connect_fake(self, event=None):
        pb = self.show_progress("Loading..", "Loading plate from configuration")

        def f():
            return self.controller.plate_connect(True)
        pb.start(f, auto_close=True, block=True)
        if pb.result:
            self.update_plate_menu(True)

    def plate_disconnect(self, event=None):
        pb = self.show_progress("Loading..", "Loading plate from configuration")
        pb.start(self.controller.plate_disconnect, auto_close=True, block=True)
        if pb.result:
            self.update_plate_menu(False)

    def update_plate_menu(self, state):
        v = "disabled" if state else "normal"
        self.get_widget("plate_menu").entryconfig("Connect", state=v)
        self.get_widget("plate_menu").entryconfig("Simulate", state=v)
        v = "normal" if state else "disabled"
        self.get_widget("plate_menu").entryconfig("Disconnect", state=v)
        self.get_widget("plate_menu").entryconfig("Light", state=v)

    def plate_light_load(self):
        result = False
        # show dialog
        fp = tkFileDialog.askopenfilename(
            filetypes=[("CSV File", "*.csv")], initialdir=os.getcwd()
        )
        if fp is not None:
            pb = self.show_progress("Loading..", "Loading intensities")

            def f():
                return self.controller.plate_load_intensities(fp, update=False)
            pb.start(f, auto_close=True, block=True)
            result = pb.result
        # do something with result
        if result:
            self.status = "Successfully loaded the light intensities"
        else:
            self.status = "Failed to load the light intensities"

    def plate_light_apply(self):
        pb = self.show_progress("Loading..", "Loading plate from configuration")
        pb.start(self.controller.plate_apply_leds, auto_close=True, block=True)
        if pb.result:
            self.update_plate_menu(False)

    def plate_light_save(self, as_=False):
        result = False
        fp = tkFileDialog.asksaveasfilename(
            initialfile="{}_intensities.csv".format(dt.now().strftime("%Y%m%d")),
            filetypes=[("CSV File", "*.csv")], initialdir=os.getcwd()
        )
        if fp is not None:
            pb = self.show_progress("Saving..", "Saving intensities")

            def f():
                return self.controller.plate_save_intensities(fp)
            pb.start(f, auto_close=True, block=True)
            result = pb.result
        # do something with result

    def update_plate_light(self, state):
        v = "normal" if state else "disabled"
        self.get_widget("plate_menu").entryconfig("Connect", state=v)
        self.get_widget("plate_menu").entryconfig("Simulate", state=v)
        v = "disabled" if state else "normal"
        self.get_widget("plate_menu").entryconfig("Disconnect", state=v)
        self.get_widget("plate_menu").entryconfig("Light", state=v)

    def console(self, event=None):
        self.controller.console()

