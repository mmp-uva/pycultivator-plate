
from julesTk import view

try:
    import tkFont
except ImportError:
    import tkinter.font as tkFont


class PlateView(view.View):
    """A representation of a Plate"""

    ORIENTATION_HORIZONTAL = 0
    ORIENTATION_VERTICAL = 1
    ORIENTATIONS = [ORIENTATION_HORIZONTAL, ORIENTATION_VERTICAL]

    def __init__(self, parent, controller, plate, **settings):
        """Initialize the Plate

        :param parent: The parent Frame
        :param controller: Controller in control of this view
        :param plate: Index or Model of the Plate
        """
        super(PlateView, self).__init__(parent=parent, controller=controller)
        self.configure(**settings)
        self._plate = plate
        self._orientation = self.ORIENTATION_VERTICAL
        # create widgets
        self._header = view.ttk.Label(self, text="Plate %s" % plate, relief=view.tk.RAISED)
        self._header.pack(side="top", fill=view.tk.X)
        self._body = view.ttk.Frame(self)
        self._body.pack(side=view.tk.BOTTOM, fill=view.tk.BOTH, expand=1)
        self._column_headers = {
            # column: widget
        }
        self._row_headers = {
            # row: widget
        }
        # dictionary to store all well widgets
        self._wells = {
            # id: widget
        }
        self._prepare_body()
        # make the layout
        self.layout()

    @property
    def orientation(self):
        return self._orientation

    @orientation.setter
    def orientation(self, orientation):
        if orientation not in self.ORIENTATIONS:
            raise ValueError("Invalid orientation value received: %s" % orientation)
        self._orientation = orientation
        # update GUI

    def isHorizontal(self):
        return self.orientation == self.ORIENTATION_HORIZONTAL

    def isVertical(self):
        return self.orientation == self.ORIENTATION_VERTICAL

    def _prepare_body(self):
        self._prepare_row_headers()
        self._prepare_column_headers()
        self._prepare_wells()

    def _prepare_row_headers(self):
        font = tkFont.Font(weight='bold')
        for row in range(4):
            label = view.ttk.Label(self._body, text=chr(65 + row), font=font)
            self._row_headers[row] = label

    def _prepare_column_headers(self):
        font = tkFont.Font(weight='bold')
        for column in range(6):
            label = view.ttk.Label(self._body, text=column, font=font)
            self._column_headers[column] = label

    def _prepare_wells(self):
        for row in range(6):
            for col in range(4):
                idx = row * 4 + col
                w = view.ttk.Label(self._body, text=idx)
                well = idx + (self._plate-1) * 24
                f = lambda event, i=well: self.process_click(i)
                w.bind("<1>", f)
                self._wells[idx] = w

    def layout(self):
        """will layout the well widgets"""
        rows = (4 if self.isHorizontal() else 6) + 1
        cols = (6 if self.isHorizontal() else 4) + 1
        self.configure_row(self._body, range(rows))
        self.configure_column(self._body, range(cols))
        self.layout_headers()
        for idx in self._wells.keys():
            if self.isHorizontal():
                row = (idx // 6) + 1
                col = (idx % 6) + 1
            else:
                row = (idx // 4) + 1
                col = (idx % 4) + 1
            well = self._wells[idx]
            well.grid(row=row, column=col)

    def layout_headers(self):
        for idx in self._column_headers.keys():
            header = self._column_headers[idx]
            row = idx + 1
            col = idx + 1
            if self.isHorizontal():
                row = 0
            else:
                col = 0
            header.grid(row=row, column=col)
        for idx in self._row_headers.keys():
            header = self._row_headers[idx]
            row = idx + 1
            col = idx + 1
            if self.isHorizontal():
                col = 0
            else:
                row = 0
            header.grid(row=row, column=col)

    def process_click(self, well):
        self.controller.open_well(well)
