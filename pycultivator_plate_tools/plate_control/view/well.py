from julesTk import view
from julesTk.utils import modals


class WellView(modals.Dialog):

    def __init__(self, parent, controller):
        super(WellView, self).__init__(parent=parent, controller=controller)
        self._status = view.tk.StringVar("")

    @property
    def status(self):
        return self._status.get()

    @status.setter
    def status(self, v):
        self._status.set(v)

    def _prepare(self):
        self.geometry('500x300+300+200')
        self.minsize(500, 300)

        self.grid_rowconfigure(1, weight=1)
        self.grid_columnconfigure(0, weight=1)

        frmh = view.ttk.Frame(self, borderwidth=1)
        frmh.grid(row=0, column=0, sticky="ew")
        self._prepare_header(frmh)

        frmb = view.ttk.Frame(self, relief=view.tk.RAISED, borderwidth=1)
        frmb.grid(row=1, column=0, sticky="nsew")
        self._prepare_body(frmb)

        frmt = view.ttk.Frame(self, relief=view.tk.RAISED, borderwidth=1)
        frmt.grid(row=2, column=0, sticky="ew")
        self._prepare_tools(frmt)

        frms = view.ttk.Frame(self, relief=view.tk.SUNKEN, borderwidth=1)
        frms.grid(row=3, column=0, sticky="ew")
        self._prepare_status(frms)
        self.bind("<Escape>", self.do_close)

    def _prepare_header(self, parent):
        pass

    def _prepare_body(self, parent):
        # from pycultivator_plate.instrument import plateLED
        leds = self.controller.well.getInstrumentsOfType("led")
        max_col = 2
        for idx, led in enumerate(leds.values()):
            lct = LEDFrame(parent, led)
            lct.configure(relief=view.tk.SUNKEN, borderwidth=1)
            row = idx // max_col
            col = idx % max_col
            lct.grid(row=row, column=col, padx=5, pady=5)

    def _prepare_led(self, parent, led):
        pass

    def _prepare_tools(self, parent):
        bta = view.ttk.Button(parent, text="Apply", command=self.do_apply)
        bta.pack(side=view.tk.RIGHT)
        bto = view.ttk.Button(parent, text="Ok", command=self.do_ok)
        bto.pack(side=view.tk.RIGHT)
        btc = view.ttk.Button(parent, text="Close", command=self.do_close)
        btc.focus_set()
        btc.pack(side=view.tk.LEFT)

    def _prepare_status(self, parent):
        lbs = view.ttk.Label(parent, textvariable=self._status)
        lbs.pack(fill=view.tk.BOTH, expand=1)

    def do_apply(self, event=None):
        pass

    def do_ok(self, event=None):
        # apply settings
        self.do_apply()
        # exit
        self.exit()

    def do_close(self, event=None):
        self.exit()


class LEDFrame(view.Frame):

    MODE_SCALE = "scale"
    MODE_DIAL = "dial"

    def __init__(self, parent, led, mode=MODE_SCALE):
        """

        :param parent:
        :type parent:
        :param led:
        :type led: pycultivator_plate.instrument.plateLED.PlateLED
        :param mode:
        :type mode:
        """
        super(LEDFrame, self).__init__(parent=parent)
        if mode not in [self.MODE_SCALE, self.MODE_DIAL]:
            raise ValueError("Invalid mode value: %s" % mode)
        self._mode = mode
        self._led = led
        self._lbi = None
        self._intensity = view.tk.StringVar()
        value = "{:0.2f}".format(led.getIntensity(False))
        self._intensity.set(value)
        self._sci = None
        self._enabled = view.tk.BooleanVar()
        self._enabled.set(led.getState(False))
        self._changed = view.tk.BooleanVar()
        self._changed.set(led.has_changed)
        self._chs = None
        self._prepare()

    @property
    def mode(self):
        return self._mode

    @property
    def enabled(self):
        return self._enabled.get()

    @enabled.setter
    def enabled(self, state):
        self._enabled.set(state is True)

    @property
    def intensity(self):
        return self._intensity.get()

    @intensity.setter
    def intensity(self, value):
        if isinstance(value, (int, float)):
            value = "{:0.2f}".format(value)
        self._intensity.set(value)

    def _prepare(self):
        # show color
        lbc = view.ttk.Label(self, text=self._led.getColorName())
        lbc.grid(row=0, column=0)
        # show intensity scale
        max_intensity = self._led.calibrate(4095)
        self._sci = view.ttk.Scale(
            self, command=self.update_intensity,
            orient=view.tk.HORIZONTAL,
            from_=0, value=self.intensity, to=max_intensity
        )
        self._sci.grid(row=1, column=0)
        # show intensity value
        tei = view.tk.Entry(self, textvariable=self._intensity)
        tei.bind("<Enter>", lambda e: self.apply_intensity(False))
        tei.bind("<FocusOut>", lambda e: self.apply_intensity(False))
        tei.grid(row=2, column=0)
        # show status checkbox
        self._chs = view.ttk.Checkbutton(self, text="Enabled", variable=self._enabled)
        self._chs.grid(row=3, column=0)
        # add frame for buttons
        fmb = view.ttk.Frame(self)
        self._prepare_buttons(fmb)
        fmb.grid(row=4, column=0)

    def _prepare_buttons(self, parent):
        # show apply button
        bta = view.ttk.Button(parent, text="Apply", command=lambda: self.apply(True))
        bta.pack(side=view.tk.LEFT, fill=view.tk.Y)
        # show reset button
        btr = view.ttk.Button(parent, text="Restore", command=self.restore)
        btr.pack(side=view.tk.LEFT, fill=view.tk.Y)

    def apply(self, commit=False):
        self.apply_intensity(commit)
        self.apply_state(commit)

    def update_intensity(self, value):
        self._intensity.set('%0.2f' % float(value))

    def apply_intensity(self, commit=False):
        value = self._intensity.get()
        value = self._led.parseToFloat(value)
        if value is not None:
            self._led.setIntensity(value, commit)

    def apply_state(self, commit=False):
        state = self._enabled.get()
        self._led.setState(state, commit)

    def restore(self):
        self.restore_state()
        self.restore_intensity()

    def restore_state(self):
        self._led.restore("state")

    def restore_intensity(self):
        self._led.restore("intensity")
