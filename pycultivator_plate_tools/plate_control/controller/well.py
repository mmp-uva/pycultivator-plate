
from julesTk import controller
from pycultivator_plate_tools.plate_control.view.well import WellView
from pycultivator.core import pcLogger


class WellController(controller.ViewController):

    VIEW_CLASS = WellView
    # class wide logger instance
    _log = None

    def __init__(self, parent, well):
        super(WellController, self).__init__(parent=parent)
        self._well = well

    @classmethod
    def getRootLog(cls):
        """Returns the root logger object used in this script

        :rtype: logging.Logger
        """
        return pcLogger.getLogger()

    @classmethod
    def getLog(cls):
        """Return the logger object of this class

        :return: The logger object connected to this class
        :rtype: logging.Logger
        """
        if cls._log is None:
            cls._log = pcLogger.createLogger(cls.__name__)
        return cls._log

    @property
    def well(self):
        """The well managed by this controller
        
        :rtype: pycultivator_plate.plate.PlateWell
        """
        return self._well

    def _start(self):
        self.view.title("Configure well {}".format(self.well.index))
        super(WellController, self)._start()

    def _stop(self):
        if self.application.has_controller("well_config"):
            self.application.remove_controller("well_config")
        super(WellController, self)._stop()
