
from julesTk import controller
from pycultivator_plate_tools.plate_control.view.plate import PlateView
from well import WellController
from pycultivator.core import pcLogger


class PlateController(controller.ViewController):

    VIEW_CLASS = PlateView
    _log = None

    def __init__(self, parent, view=None):
        """Initialize Controller with
        
        :param parent: Parent controller
        :param view: View 
        """
        super(PlateController, self).__init__(parent=parent, view=view)

    @classmethod
    def getRootLog(cls):
        """Returns the root logger object used in this script

        :rtype: logging.Logger
        """
        return pcLogger.getLogger()

    @classmethod
    def getLog(cls):
        """Return the logger object of this class

        :return: The logger object connected to this class
        :rtype: logging.Logger
        """
        if cls._log is None:
            cls._log = pcLogger.createLogger(cls.__name__)
        return cls._log

    def _prepare(self):
        super(PlateController, self)._prepare()
        # load more
        return self

    def _start(self):
        super(PlateController, self)._start()
        self.update_view()

    def update_view(self):
        pass

    def open_well(self, idx):
        """Handle click input of a well"""
        # self.getLog().info("Clicked on well {}".format(idx))
        device = None
        well = None
        if self.parent.hasDevice():
            device = self.parent.device
        else:
            self.parent_view.status = "No plate loaded, open configuration first!"

        if device is not None:
            if not self.parent.device.isConnected():
                self.parent_view.status = "Not connected to plate, connect first!"
            elif self.parent.device.hasWell(idx):
                well = self.parent.device.getWell(idx)

        if well is not None:
            if not self.application.has_controller("well_config"):
                c = WellController(self, well)
                self.application.add_controller("well_config", c)
            c = self.application.get_controller("well_config")
            c.start()
