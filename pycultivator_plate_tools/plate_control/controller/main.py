from julesTk import controller
from julesTk.utils.observe import Observer
from pycultivator.core import pcLogger, pcException
from pycultivator_plate_tools.plate_control.view.main import MainView
from console import ConsoleController
import os


class MainController(controller.ViewController, Observer):

    VIEW_CLASS = MainView
    # class wide logger instance
    _log = None

    def __init__(self, parent, view=None):
        super(MainController, self).__init__(parent=parent, view=view)
        self._config = None
        self._device = None

    @classmethod
    def getRootLog(cls):
        """Returns the root logger object used in this script

        :rtype: logging.Logger
        """
        return pcLogger.getLogger()

    @classmethod
    def getLog(cls):
        """Return the logger object of this class

        :return: The logger object connected to this class
        :rtype: logging.Logger
        """
        if cls._log is None:
            cls._log = pcLogger.createLogger(cls.__name__)
        return cls._log

    def _prepare(self):
        super(MainController, self)._prepare()
        # load more
        return self

    def _start(self):
        super(MainController, self)._start()
        self.update_view()

    def _stop(self):
        if self.hasDevice() and self.device.isConnected():
            self.plate_disconnect()
        super(MainController, self)._stop()

    def getConfig(self):
        """Get configuration object

        :rtype: pycultivator_plate.config.baseConfig.Config
        """
        return self._config

    def hasConfig(self):
        return self.getConfig() is not None

    @property
    def config(self):
        return self.getConfig()

    def getDevice(self):
        """The device loaded into memory

        :rtype: pycultivator_plate.plate.Plate
        """
        return self._device

    def hasDevice(self):
        return self.getDevice() is not None

    @property
    def device(self):
        return self.getDevice()

    def update(self, observable):
        pass

    def update_view(self):
        pass

    def open_config(self, fp):
        result = False
        config = None
        msg = "Failed to open the configuration file"
        # if fp is None; try to load config
        if fp is not None:
            config = self.load_config(fp)
            result = config is not None
        # check if we have a loaded a configuration and it's new
        if result and config != self.config:
            # disconnect before replacing the config and device tree
            if self.hasDevice() and self.device.isConnected():
                self.plate_disconnect()
            self._config = config
            self._device = None
            result = self.load_device()
            if result:
                msg = "Successfully loaded: %s" % os.path.basename(fp)
        self.view.status = msg
        return result

    def load_config(self, fp):
        if not os.path.exists(fp):
            raise IOError("Unknown File Path: %s" % fp)
        from pycultivator.config import xmlConfig
        result = None
        try:
            result = xmlConfig.XMLConfig.load(fp)
        except xmlConfig.XMLConfigException as xce:
            self.getLog().warning(xce, exc_info=1)
        return result

    def load_device(self, use_fake=True):
        if self.hasConfig():
            # now load device
            self.config.setSetting("fake.connection", use_fake, namespace="")
            xdc = self.config.getHelperFor("device", settings={"fake.connection": use_fake})
            device = None
            try:
                device = xdc.load()
            except pcException.PCException as pce:
                self.getLog().error(pce, exc_info=1)
            self._device = device
        return self.hasDevice()

    def save_config(self, fp):
        result = False
        # do stuff
        return result

    def plate_connect(self, use_fake=False):
        result = False
        msg = "Unable to connect to plate"
        if self.hasDevice():
            if not self.device.isConnected():
                self.device.createConnection(use_fake)
                result = self.device.connect(use_fake)
                if result:
                    self.plate_commit_leds()
                    msg = "Successfully connected to plate"
            else:
                msg = "Already connected to plate"
                result = True
        # show message
        self.view.status = msg
        return result

    def plate_load_intensities(self, fp, update=True):
        result = False
        rows = []
        fields = set()
        # open csv
        if os.path.exists(fp):
            from csv import DictReader
            with open(fp, "r") as src:
                r = DictReader(src)
                rows = [row for row in r]
                fields = r.fieldnames
        # lower case all fields
        fields = map(str.lower, fields)
        # check and process
        result = self.hasDevice()
        if not result:
            self.getLog().warning("No device object loaded")
        result = self._check_fields(fields, ["well", "color", "intensity"])
        if not result:
            self.getLog().warning("Fields missing in CSV File")
        if result:
            result = len(rows) > 0
            for row in rows:
                well_idx = self.device.parseToInteger(row['well'], None)
                if well_idx is not None and self.device.hasWell(well_idx):
                    well = self.device.getWell(well_idx)
                    result = self._process_well(
                        well, row.get("led", None), row["color"], row["intensity"], update=update
                    ) and result
        return result

    def plate_commit_leds(self):
        """Commit to the current value of all leds"""
        result = False
        if self.hasDevice() and self.device.isConnected():
            for well in self.device.getWells().values():
                leds = well.getLEDs()
                for led in leds.values():
                    led.commit()
                    result = not led.has_changed and result
        return result

    def plate_reset_leds(self):
        result = False
        if self.hasDevice() and self.device.isConnected():
            for well in self.device.getWells().values():
                leds = well.getLEDs()
                for led in leds.values():
                    result = led.setIntensity(0, True) and result
                    result = led.setState(False, True) and result
        return result

    def plate_apply_leds(self, only_new=True):
        result = False
        if self.hasDevice() and self.device.isConnected():
            for well in self.device.getWells().values():
                leds = well.getLEDs()
                for led in leds.values():
                    if not only_new or led.has_changed:
                        led.apply()
        return result

    def _check_fields(self, fields, required_fields):
        result = True
        for name in required_fields:
            result = name in fields
            if not result:
                break
        return result

    def _process_well(self, well, led, color, intensity, update=True):
        """

        :param well:
        :type well: pycultivator_plate.plate.PlateWell
        :param led:
        :type led:
        :param color:
        :type color:
        :param intensity:
        :type intensity:
        :return:
        :rtype: bool
        """
        result = False
        if led is not None:
            led = well.getInstrument(led) if well.hasInstrument(led) else None
            """:type: pycultivator_plate.instrument.plateLED.PlateLED"""
        intensity = well.parseToFloat(intensity, default=None)
        if intensity is not None:
            if led is not None:
                result = self.configure_led_intensity(led, intensity, update=update)
            else:
                leds = well.getLEDsOfColor(color)
                result = True
                for led in leds.values():
                    result = self.configure_led_intensity(led, intensity, update=update) and result
        return result

    def configure_led_state(self, led, state, update=False):
        return led.setState(state, update=update)

    def configure_led_intensity(self, led, intensity, update=False):
        result = False
        if led.testIntensity(intensity):
            result = led.setIntensity(intensity, update=update)
        else:
            self.getLog().warning("Intensity {} is out of range".format(intensity))
        return result

    def plate_save_intensities(self, fp):
        result = False
        # construct list[dict]
        rows = []
        fields = ['well', 'led', 'color', 'intensity']
        if self.hasDevice():
            for well in self.device.getWells().values():
                for led in well.getLEDs().values():
                    rows.append({
                        'well': well.index, 'led': led.identity,
                        'color': led.getColorName(),
                        'intensity': led.getIntensity()
                    })
        # write csv
        if len(rows) > 0 and os.path.exists(os.path.dirname(fp)):
            from csv import DictWriter
            with open(fp, "w") as dest:
                w = DictWriter(dest, fieldnames=fields)
                w.writeheader()
                w.writerows(rows)
            result = True
        return result

    def configure_well_intensity(self, well, color, intensity, update=True):
        result = False
        if self.hasDevice() and self.device.isConnected():
            if isinstance(well, int):
                if self.device.hasWell(well):
                    well = self.device.getWell(well)
                else:
                    well = None
            if well is not None:
                leds = well.getLEDsOfColor(color)
                for led in leds.values():
                    led.setIntensity(intensity, update=update)
        return result

    def configure_well_state(self, well, color, state, update=True):
        result = False
        if self.hasDevice() and self.device.isConnected():
            if isinstance(well, int):
                if self.device.hasWell(well):
                    well = self.device.getWell(well)
                else:
                    well = None
            if well is not None:
                leds = well.getLEDsOfColor(color)
                for led in leds.values():
                    led.setState(state, update=update)
        return result

    def plate_disconnect(self):
        result = False
        msg = "Unable to disconnect from plate"
        if self.hasDevice():
            if self.device.isConnected():
                result = self.device.disconnect()
                if result:
                    msg = "Successfully disconnected from plate"
            else:
                msg = "Already disconnected from plate"
        # show message
        self.view.status = msg
        return result

    def console(self):
        if not self.application.has_controller("console"):
            console = ConsoleController(self.application)
            self.application.add_controller("console", console)
            console.start()
        console = self.application.get_controller("console")
        """:type: julesTk.controller.ViewController"""
        console.view.show()
