from julesTk.app import Application
from controller.main import MainController
from pycultivator.core import pcLogger


class PlateControl(Application):
    """Run the PlateControl Application
    
    - Use the `run` method to load and execute the application.
    
    """

    # class wide logger instance
    _log = None

    def __init__(self):
        super(PlateControl, self).__init__()

    @classmethod
    def getRootLog(cls):
        """Returns the root logger object used in this script

        :rtype: logging.Logger
        """
        return pcLogger.getLogger()

    @classmethod
    def getLog(cls):
        """Return the logger object of this class

        :return: The logger object connected to this class
        :rtype: logging.Logger
        """
        if cls._log is None:
            cls._log = pcLogger.createLogger(cls.__name__)
        return cls._log

    def _prepare(self):
        self.title('24-Well Plate Control')
        self.geometry('500x500+200+200')
        self.minsize(500, 300)
        self.add_controller("main", MainController(self))

    @property
    def main(self):
        return self.get_controller("main")

    def _start(self):
        self.main.start()


def main():
    app = PlateControl()
    # connect to
    # log_handler = pcLogger.UVAStreamHandler()
    # log_handler.setLevel(20)  # set to info
    # app.getRootLog().addHandler(log_handler)
    # app.getRootLog().setLevel(20)
    app.run()


if __name__ == "__main__":
    main()
